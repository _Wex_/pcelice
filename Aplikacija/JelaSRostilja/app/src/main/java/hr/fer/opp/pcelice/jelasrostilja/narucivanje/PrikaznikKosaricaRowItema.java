package hr.fer.opp.pcelice.jelasrostilja.narucivanje;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hr.fer.opp.pcelice.jelasrostilja.R;

public class PrikaznikKosaricaRowItema extends ArrayAdapter<KosaricaRowItem> {

    Context context;

    public PrikaznikKosaricaRowItema(Context context, int resourceId,
                                     List<KosaricaRowItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView slikaJela;
        TextView naslov;
        TextView kolicina;
        TextView ukupno;
        ImageButton plusButton;
        ImageButton minusButton;
        ImageButton makniJeloButton;
        TextView karakteristike;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        KosaricaRowItem kosaricaRowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.kosarica_list_item, null);
            holder = new ViewHolder();
            holder.kolicina = (TextView) convertView.findViewById(R.id.kosaricaItemKolicina);
            holder.naslov = (TextView) convertView.findViewById(R.id.kosaricaItemNaslov);
            holder.slikaJela = (ImageView) convertView.findViewById(R.id.kosaricaItemSlika);
            holder.ukupno = (TextView) convertView.findViewById(R.id.kosaricaUkupno);
            holder.plusButton = (ImageButton) convertView.findViewById(R.id.kosaricaPovecajKolicinuJelaButton);
            holder.minusButton = (ImageButton) convertView.findViewById(R.id.kosaricaSmanjiKolicinuJelaButton);
            holder.makniJeloButton = (ImageButton) convertView.findViewById(R.id.kosaricaMakniJeloButton);
            holder.karakteristike = (TextView) convertView.findViewById(R.id.kosaricaItemMogucnosti);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.kolicina.setText("Kolicina: " + kosaricaRowItem.getKolicina());
        holder.naslov.setText(kosaricaRowItem.getNaslov());
        holder.slikaJela.setImageResource(kosaricaRowItem.getSlikaID());
        holder.ukupno.setText("Ukupno: " + kosaricaRowItem.getUkupno());
        String karakteristike = "";
        for(String kar : kosaricaRowItem.getJelo().getKarakteristike()){
            karakteristike += kar + "-";
        }
        holder.karakteristike.setText("Opcije: " + karakteristike);

        return convertView;
    }
}