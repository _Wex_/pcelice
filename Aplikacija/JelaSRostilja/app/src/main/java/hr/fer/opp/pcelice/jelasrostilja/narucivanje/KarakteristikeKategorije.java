package hr.fer.opp.pcelice.jelasrostilja.narucivanje;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Jelo;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Kategorija;

/**
 * Created by luka on 17.01.16..
 */
public class KarakteristikeKategorije {
    // za svaku kategoriju (kategorija id) postoje odredjene mogucnostiRowItems
    private static Map<Integer, List<String>> karakteristike = new HashMap<>();

    public static List<String> vratiKarakteristikeJela(Jelo jelo){
        return karakteristike.get(jelo.getKategorijaID());
    }

    public static List<String> vratiKarakteristikeKategorije(int id){
        return karakteristike.get(id);
    }

    public static void ubaciKarakteristikeKategorije(Kategorija kategorija, List<String> kar){
        karakteristike.put(kategorija.getId(), kar);
    }

}
