package hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Murta on 30.12.2015..
 */
public class Jelo {

    private Integer id;
    private String ime;
    private int kategorijaID;
    private boolean dostupno;
    //private int zadnjaIzmjena;
    private String zadnjaIzmjena;
    private String opisJela;
    private double cijena;
    private int slikaID;
    private List<String> karakteristike;
    private int brojNarucivanja;

    public Jelo (String ime, String opisJela, double cijena, int kategorijaID, boolean dostupno, String zadnjaIzmjena,
                 int slikaID, int brojNarucivanja){
        this(null, ime, opisJela, cijena, kategorijaID, dostupno, zadnjaIzmjena, slikaID, brojNarucivanja);
    }

    public Jelo (Integer id, String ime, String opisJela, double cijena, int kategorijaID, boolean dostupno, String zadnjaIzmjena, int slikaID, int brojNarucivanja){
        if (ime.length() > 20) {
            //TODO ANTON: duljinu treba provjeravati u odgovarajucoj klasi, ovdje se nemoze dojavit pogreska
        }
        if(id == null){
            this.id = null;
        }
        else{
            this.id = new Integer(id);
        }
        this.ime = ime;
        this.opisJela = opisJela;
        this.cijena = cijena;
        this.slikaID = slikaID;
        this.kategorijaID = kategorijaID;
        this.dostupno = dostupno;
        this.zadnjaIzmjena = zadnjaIzmjena;
        this.karakteristike = new LinkedList<String>();
        this.brojNarucivanja = brojNarucivanja;
    }

    public Jelo (Integer id, String ime, String opisJela, double cijena, int kategorijaID, boolean dostupno,
                 String zadnjaIzmjena, int slikaID, List<String> karakteristike, int brojNarucivanja){
        if (ime.length() > 20) {
            //TODO ANTON: duljinu treba provjeravati u odgovarajucoj klasi, ovdje se nemoze dojavit pogreska
        }
        if(id == null){
            this.id = null;
        }
        else{
            this.id = new Integer(id);
        }
        this.ime = ime;
        this.opisJela = opisJela;
        this.cijena = cijena;
        this.slikaID = slikaID;
        this.kategorijaID = kategorijaID;
        this.dostupno = dostupno;
        this.zadnjaIzmjena = zadnjaIzmjena;
        this.karakteristike = new LinkedList<String>();
        this.dodajSveKarakteristike(karakteristike);
        this.brojNarucivanja = brojNarucivanja;
    }

    public Integer getId() {
        return id;
    }

    public String getIme() {
        return ime;
    }

    public String getOpisJela() { return opisJela; }

    public double getCijena() { return cijena; }

    public int getKategorijaID() {
        return kategorijaID;
    }

    public boolean jeDostupno() {
        return dostupno;
    }

    public String getZadnjaIzmjena() {
        return zadnjaIzmjena;
    }

    public int getSlikaID() {return slikaID; }

    public List<String> getKarakteristike(){
        return karakteristike;
    }

    public void dodajKarakteristiku(String kar){
        karakteristike.add(kar);
    }

    public void dodajSveKarakteristike(List<String> kar){
        karakteristike.addAll(kar);
    }

    public int getBrojNarucivanja() {
        return brojNarucivanja;
    }

    public void novaCijena(double cijena){
        this.cijena = cijena;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public boolean equals(Object o) {
        Jelo other = null;
        try{
            other = (Jelo) o;
        }catch (Exception e){
            return false;
        }
        return this.ime.equals(other.ime) && this.karakteristike.equals(other.karakteristike);
    }

    @Override
    public String toString(){
        return this.getIme();
    }
}
