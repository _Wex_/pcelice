package hr.fer.opp.pcelice.jelasrostilja.bazapodataka;

import android.provider.BaseColumns;

/**
 * Created by Murta on 29.12.2015..
 */
public abstract class TableInfo implements BaseColumns{

    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String DATABASE = "database";
    public final String TABLENAME;

    public TableInfo(String TABLENAME){
        this.TABLENAME = TABLENAME;
    }

    public String getTABLENAME(){
        return TABLENAME;
    }

}
