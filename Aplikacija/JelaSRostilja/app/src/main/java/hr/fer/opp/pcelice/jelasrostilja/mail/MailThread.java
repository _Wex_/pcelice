package hr.fer.opp.pcelice.jelasrostilja.mail;

import android.util.Log;

/**
 * Created by luka on 18.01.16..
 */
public class MailThread implements Runnable {

    private String subject;
    private String body;
    private String clientAdress;

    public MailThread(String subject, String body, String clientAdress) {
        this.subject = subject;
        this.body = body;
        this.clientAdress = clientAdress;
    }

    @Override
    public void run() {
        try {
            GMailSender sender = new GMailSender("wild8restoran@gmail.com", "pcelice123");
            sender.sendMail(subject, body, "wild8restoran@gmail.com", clientAdress);
        } catch (Exception e) {
            System.out.println("Greska u slanju maila.");
        }
    }
}
