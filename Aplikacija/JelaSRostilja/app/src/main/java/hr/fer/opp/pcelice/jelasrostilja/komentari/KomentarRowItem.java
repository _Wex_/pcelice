package hr.fer.opp.pcelice.jelasrostilja.komentari;

import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Recenzija;

public class KomentarRowItem {
    private Recenzija recenzija;

    public KomentarRowItem(Recenzija recenzija) {
        this.recenzija = recenzija;
    }


    @Override
    public String toString() {
        return recenzija.getSadrzaj();
    }

    public Recenzija getRecenzija() {
        return recenzija;
    }

    public void setRecenzija(Recenzija recenzija) {
        this.recenzija = recenzija;
    }
}