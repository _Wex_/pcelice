package hr.fer.opp.pcelice.jelasrostilja.narucivanje;

import java.text.DecimalFormat;

import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Jelo;

public class KosaricaRowItem {
    private Jelo jelo;
    private Integer kolicina;
    private DecimalFormat formatter = new DecimalFormat("#.00 kn");

    public KosaricaRowItem(Jelo jelo, int kolicina) {
        this.jelo = jelo;
        this.kolicina = kolicina;
    }

    public int getSlikaID() {
        return jelo.getSlikaID();
    }
    public String getNaslov() {
        return (jelo.getIme());
    }
    public Jelo getJelo() { return jelo; }
    public int getKolicina() { return kolicina; }
    public void setKolicina(int novaKolicina) { this.kolicina = novaKolicina; }
    public String getUkupno() { return formatter.format(kolicina*jelo.getCijena());}

    @Override
    public String toString() {
        return this.getNaslov();
    }
}