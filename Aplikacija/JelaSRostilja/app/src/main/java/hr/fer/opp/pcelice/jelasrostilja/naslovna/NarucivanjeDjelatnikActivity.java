package hr.fer.opp.pcelice.jelasrostilja.naslovna;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import hr.fer.opp.pcelice.jelasrostilja.R;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.PrivremenaBaza;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Jelo;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Kategorija;
import hr.fer.opp.pcelice.jelasrostilja.narucivanje.KarakteristikeKategorije;
import hr.fer.opp.pcelice.jelasrostilja.narucivanje.KategorijaRowItem;
import hr.fer.opp.pcelice.jelasrostilja.narucivanje.PrikaznikKategorijaRowItema;

public class NarucivanjeDjelatnikActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private int mod = 1;
    private int position = 0;
    private String trenutniDjelatnik = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        trenutniDjelatnik = getIntent().getStringExtra("djelatnik");
        initKategorije();
    }

    private void onItemClickHelper(){
        if(mod == 1){
            Kategorija kliknuta = kategorijaRowItems.get(position).getKategorija();
            kategorijaJela = kliknuta.getId();
            sveKategorijeJela = false;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    initJela();
                }
            });
            return;
        } else if(mod == 2){
            final Jelo kliknuto = jeloRowItems.get(position).getJelo();
            if(kliknuto.jeDostupno()){
                PrivremenaBaza.baza.postaviJeloNedostupno(kliknuto.getIme());
            } else {
                PrivremenaBaza.baza.postaviJeloDostupno(kliknuto.getIme());
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Promjenjena dostupnost jela: " + kliknuto.getIme(),
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();
                    initJela();
                }
            });
        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        this.position = position;
        Thread dostupnostThread = new Thread(new Runnable() {
            @Override
            public void run() {
                onItemClickHelper();
            }
        });
        dostupnostThread.start();
    }

    @Override
    public void onBackPressed(){
        if(mod == 1){
            finish();
        } else if (mod == 2){
            initKategorije();
        }
    }
    /*--------------KATEGORIJE---------------*/
    ListView listViewKategorije;
    List<KategorijaRowItem> kategorijaRowItems;
    public PrikaznikKategorijaRowItema adapterKategorije;
    private void initKategorije(){
        setContentView(R.layout.prikaz_kategorija_djelatnik);
        mod = 1;
        kategorijaRowItems = new ArrayList<KategorijaRowItem>();
        Kategorija[] kategorije = PrivremenaBaza.baza.vratiSveKategorije();
        for(Kategorija kategorija : kategorije){
            kategorijaRowItems.add(new KategorijaRowItem(kategorija));
        }
        listViewKategorije = (ListView) findViewById(R.id.kategorijaListaKategorija);
        adapterKategorije = new PrikaznikKategorijaRowItema(this,
                R.layout.kategorija_list_item, kategorijaRowItems);
        listViewKategorije.setAdapter(adapterKategorije);
        listViewKategorije.setOnItemClickListener(this);
    }

    public void dodajKategorijuButton(View view){
        dodajKategorijuProcess();
    }

    private PopupWindow dodajKategorijuPopup = null;
    private Button buttonPohrani = null;
    private Button buttonPonisti = null;
    private EditText editTextImeKategorije = null;
    private EditText editTextOpisKategorije = null;
    private View dodajKategorijuPopupView = null;
    private void dodajKategorijuProcess() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dodajKategorijuPopupView = inflater.inflate(R.layout.dodaj_kategoriju_popup_window, (ViewGroup) findViewById(R.id.linearLayoutDKPW));
        dodajKategorijuPopup = new PopupWindow(dodajKategorijuPopupView, 100, 100, true);
        dodajKategorijuPopup.showAtLocation(this.findViewById(R.id.linearLayoutPKD), Gravity.CENTER, 0, 0);
        dodajKategorijuPopupView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        dodajKategorijuPopup.update(findViewById(R.id.linearLayoutPKD).getWidth(), dodajKategorijuPopupView.findViewById(R.id.linearLayoutDKPW).getMeasuredHeight());
        dodajKategorijuPopup.getContentView().setFocusableInTouchMode(true);
        dodajKategorijuPopup.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dodajKategorijuPopup.dismiss();
                    return true;
                }
                return false;
            }
        });
        editTextImeKategorije = (EditText) dodajKategorijuPopupView.findViewById(R.id.editTextImeKategorijeDKPW);
        editTextOpisKategorije = (EditText) dodajKategorijuPopupView.findViewById(R.id.editTextOpisKategorijeDKPW);
    }
    public void pohraniButton(View view){
        if(provjeriInformacijeKategorije()) {
            PrivremenaBaza.baza.ubaciKategoriju(editTextImeKategorije.getText().toString(),trenutniDjelatnik,editTextOpisKategorije.getText().toString(),0);
            dodajKategorijuPopup.dismiss();
            initKategorije();
        } else {
            prijavaGreske("Ime kategorije već postoji ili nije ispravno.");
        }
    }
    public void ponistiButton(View view){
        dodajKategorijuPopup.dismiss();
    }


    private boolean provjeriInformacijeKategorije(){
        if(editTextImeKategorije.getText().length()>0 && editTextOpisKategorije.getText().length()>0){
            for(Kategorija trenutnaKategorija : PrivremenaBaza.baza.vratiSveKategorije()){
                if(trenutnaKategorija.getIme().equals(editTextImeKategorije.getText().toString())){
                    return false;
                }
            }
            return true;
        }
        return false;
    }


    private PopupWindow errorPopup = null;
    private View errorPopupView = null;
    private TextView textViewError = null;
    private Button buttonBack = null;
    private void prijavaGreske(String greska){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        errorPopupView = inflater.inflate(R.layout.error_popup_window,(ViewGroup)findViewById(R.id.linearLayoutErrorPopup));
        errorPopup = new PopupWindow(errorPopupView, 100, 100, true);
        if(mod==1){errorPopup.showAtLocation(this.findViewById(R.id.linearLayoutPKD), Gravity.CENTER, 0, 0);}
        else {errorPopup.showAtLocation(this.findViewById(R.id.linearLayout_APJ), Gravity.CENTER, 0, 0);}
        errorPopup.getContentView().setFocusableInTouchMode(true);
        errorPopup.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    errorPopup.dismiss();
                    return true;
                }
                return false;
            }
        });
        errorPopupView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        if(mod==1){errorPopup.update(findViewById(R.id.linearLayoutPKD).getWidth(), errorPopupView.findViewById(R.id.linearLayoutErrorPopup).getMeasuredHeight());}
        else {errorPopup.update(findViewById(R.id.linearLayout_APJ).getWidth(), errorPopupView.findViewById(R.id.linearLayoutErrorPopup).getMeasuredHeight());}
        textViewError = (TextView)errorPopupView.findViewById(R.id.textViewErrorEPW);
        textViewError.setText(greska);
        buttonBack = (Button)errorPopupView.findViewById(R.id.buttonBackEPW);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorPopup.dismiss();
            }
        });
    }


    /*----------------HRANA------------------*/
    public PrikaznikDjelatnikJeloRowItema adapterJela;
    ListView listViewJela;
    List<DjelatnikJeloRowItem> jeloRowItems;

    private int kategorijaJela = 0;
    private boolean sveKategorijeJela = false;
    private Button buttonDodaj = null;
    public void stisnutGumbKosariceZaJelo(View view){}
    private void initJela(){
        setContentView(R.layout.djelatnik_prikaz_jela);
        Jelo[] dostupnaJela = PrivremenaBaza.baza.vratiSvaJela();
        jeloRowItems = new ArrayList<DjelatnikJeloRowItem>();
        mod = 2;
        for (Jelo jelo : dostupnaJela) {
            int id = jelo.getKategorijaID();
            if(jelo.getKategorijaID() == kategorijaJela){
                DjelatnikJeloRowItem item = new DjelatnikJeloRowItem(jelo);
                jeloRowItems.add(item);
            }
        }
        listViewJela = (ListView) findViewById(R.id.djelatnikJelaListaJela);
        adapterJela = new PrikaznikDjelatnikJeloRowItema(this,
                R.layout.djelatnik_jela_list_item, jeloRowItems);
        listViewJela.setAdapter(adapterJela);
        listViewJela.setOnItemClickListener(this);

        buttonDodaj = (Button)findViewById(R.id.djelatnikJelaPrikazKosariceButton);
        buttonDodaj.setText("DODAJ");
        buttonDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dodajJeloProcess();
            }
        });
    }

    private PopupWindow dodajJeloPopup = null;
    private EditText editTextImeJela = null;
    private EditText editTextOpisJela = null;
    private EditText editTextCijenaJela = null;
    private View dodajJeloPopupView = null;
    private void dodajJeloProcess() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dodajJeloPopupView = inflater.inflate(R.layout.dodaj_jelo_popup, (ViewGroup) findViewById(R.id.linearLayout_DJP));
        dodajJeloPopup = new PopupWindow(dodajJeloPopupView, 100, 100, true);
        dodajJeloPopup.showAtLocation(this.findViewById(R.id.linearLayout_APJ), Gravity.CENTER, 0, 0);
        dodajJeloPopupView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        dodajJeloPopup.update(findViewById(R.id.linearLayout_APJ).getWidth(), dodajJeloPopupView.findViewById(R.id.linearLayout_DJP).getMeasuredHeight());
        dodajJeloPopup.getContentView().setFocusableInTouchMode(true);
        dodajJeloPopup.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dodajJeloPopup.dismiss();
                    return true;
                }
                return false;
            }
        });
        editTextImeJela = (EditText) dodajJeloPopupView.findViewById(R.id.editTextImeJela_DJP);
        editTextOpisJela = (EditText) dodajJeloPopupView.findViewById(R.id.editTextOpisJela_DJP);
        editTextCijenaJela = (EditText) dodajJeloPopupView.findViewById(R.id.editTextCijena_DJP);
    }
    public void pohraniButtonDJP(View view){
        if(provjeriInformacijeJelo()) {
            if(selectedImageUri != null) {
                boolean postoji = false;
                int slikaID = 0;
                String slikaSpremi;
                Random random = new Random();
                do {
                    postoji = false;
                    slikaID = random.nextInt();
                    slikaSpremi = "slika" + slikaID;
                    if (PrivremenaBaza.baza.procitajInformaciju(slikaSpremi) != null) {
                        postoji = true;
                    }
                } while (postoji);
                PrivremenaBaza.baza.spremiInformaciju(slikaSpremi, selectedImageUri.toString());
                if(odabraneKarakteristike != null){
                    PrivremenaBaza.baza.ubaciJelo(editTextImeJela.getText().toString(), editTextOpisJela.getText().toString(), Double.valueOf(editTextCijenaJela.getText().toString()), PrivremenaBaza.baza.vratiImeKategorijeZaID(kategorijaJela), true, slikaID, trenutniDjelatnik, 0, odabraneKarakteristike);
                } else {
                    PrivremenaBaza.baza.ubaciJelo(editTextImeJela.getText().toString(), editTextOpisJela.getText().toString(), Double.valueOf(editTextCijenaJela.getText().toString()), PrivremenaBaza.baza.vratiImeKategorijeZaID(kategorijaJela), true, slikaID, trenutniDjelatnik, 0);
                }
            } else {
                if(odabraneKarakteristike != null) {
                    PrivremenaBaza.baza.ubaciJelo(editTextImeJela.getText().toString(), editTextOpisJela.getText().toString(), Double.valueOf(editTextCijenaJela.getText().toString()), PrivremenaBaza.baza.vratiImeKategorijeZaID(kategorijaJela), true, 0, trenutniDjelatnik, 0, odabraneKarakteristike);
                } else {
                    PrivremenaBaza.baza.ubaciJelo(editTextImeJela.getText().toString(), editTextOpisJela.getText().toString(), Double.valueOf(editTextCijenaJela.getText().toString()), PrivremenaBaza.baza.vratiImeKategorijeZaID(kategorijaJela), true, 0, trenutniDjelatnik, 0);
                }
            }
            dodajJeloPopup.dismiss();
            initJela();
        } else {
            prijavaGreske("Ime jela već postoji ili nije ispravno.");
        }
    }

    public void ponistiButtonDJP(View view){
        dodajJeloPopup.dismiss();
    }

    private boolean provjeriInformacijeJelo(){
        if(editTextImeJela.getText().toString().length()<1 || editTextImeJela.getText().toString().length()>20){
            return false;
        }
        for(Jelo jelo : PrivremenaBaza.baza.vratiSvaJela()){
            if(editTextImeJela.getText().toString().equals(jelo.getIme())){
                return false;
            }
        }
        if(editTextOpisJela.getText().toString().length()<1 || editTextOpisJela.getText().toString().length()>50){
            return false;
        }
        try{
            Double cijena = Double.valueOf(editTextCijenaJela.getText().toString());
            if(cijena<=0){
                return false;
            }
        } catch (Exception e){
            return false;
        }
        return true;
    }


    private static final int SELECT_PICTURE = 1;
    public void odaberiSliku(View v){
        Intent fileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        fileIntent.setType("image/*");
        fileIntent.setAction(Intent.ACTION_GET_CONTENT);
        try{
            startActivityForResult(Intent.createChooser(fileIntent, "Odaberi sliku."), SELECT_PICTURE);
        } catch(Exception e) {}
    }
    private Uri selectedImageUri = null;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK){
            if(requestCode == SELECT_PICTURE){
                selectedImageUri = data.getData();
            }
        }
    }

    public void karakteristike(View view){
        karakteristikeProcess();
    }
    public void karakteristikePopupDismiss(View view){
        karakteristikePopup.dismiss();
    }
    String[] odabraneKarakteristike = null;
    public void pohraniKarakteristike(View view){
        odabraneKarakteristike = null;
        int brojOdabranihKar = 0;
        for(KarakteristikaDisplay trenutni : listaDisplayaKarakteristika){
            if (trenutni.getChecked()){
                brojOdabranihKar++;
            }
        }
        if(brojOdabranihKar>0) {
            odabraneKarakteristike = new String[brojOdabranihKar];
        } else {
            karakteristikePopup.dismiss();
            return;
        }
        int i = 0;
        for(KarakteristikaDisplay trenutni : listaDisplayaKarakteristika){
            if (trenutni.getChecked()){
                odabraneKarakteristike[i] = trenutni.getKarakteristika();
                ++i;
            }
        }
        karakteristikePopup.dismiss();
    }
    private PopupWindow karakteristikePopup = null;
    private View karakteristikePopupView = null;
    private void karakteristikeProcess() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        karakteristikePopupView = inflater.inflate(R.layout.karakteristike_popup, (ViewGroup) findViewById(R.id.linearLayoutKarMain));
        karakteristikePopup = new PopupWindow(karakteristikePopupView, 100, 100, true);
        karakteristikePopup.showAtLocation(this.findViewById(R.id.linearLayout_APJ), Gravity.CENTER, 0, 0);
        karakteristikePopupView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        linearLayoutKarakteristikeMain = (LinearLayout)karakteristikePopupView.findViewById(R.id.linearLayoutKarMain);
        ponudiKarakteristike();
        karakteristikePopup.update(findViewById(R.id.linearLayout_APJ).getWidth(), findViewById(R.id.linearLayout_APJ).getHeight());
        karakteristikePopup.getContentView().setFocusableInTouchMode(true);
        karakteristikePopup.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    karakteristikePopup.dismiss();
                    return true;
                }
                return false;
            }
        });
        if(listaDisplayaKarakteristika.isEmpty()){karakteristikePopup.dismiss();}
    }
    private LinkedList<KarakteristikaDisplay> listaDisplayaKarakteristika = new LinkedList<>();
    private LinearLayout linearLayoutKarakteristikeMain = null;
    private void ponudiKarakteristike(){
        List<String> sveKarakteristike = PrivremenaBaza.baza.vratiSveKarakteristike();
        for(String karakteristika : sveKarakteristike){
            if(karakteristika.equals("")){return;}
            KarakteristikaDisplay novi = new KarakteristikaDisplay(karakteristika);
            listaDisplayaKarakteristika.add(novi);
        }
        if(listaDisplayaKarakteristika.isEmpty()){return;}
    }
    private class KarakteristikaDisplay{
        private LinearLayout linLayKarakteristika;
        private TextView imeKarakteristike;
        private String karakteristika;
        private CheckBox checkBoxKarakteristike;
        public boolean getChecked(){
            return checkBoxKarakteristike.isChecked();
        }
        public String getKarakteristika(){
            return karakteristika;
        }
        KarakteristikaDisplay(String karakteristika){
            this.karakteristika = karakteristika;
            linLayKarakteristika = new LinearLayout(getApplicationContext());
            linLayKarakteristika.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            linLayKarakteristika.setOrientation(LinearLayout.HORIZONTAL);
            linLayKarakteristika.setWeightSum(2);

            imeKarakteristike = new TextView(getApplicationContext());
            imeKarakteristike.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            imeKarakteristike.setText(karakteristika);
            imeKarakteristike.setTextSize(14);
            imeKarakteristike.setTextColor(Color.BLACK);
            LinearLayout linLayIme = new LinearLayout(getApplicationContext());
            linLayIme.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.7f));
            linLayIme.addView(imeKarakteristike);

            checkBoxKarakteristike = new CheckBox(getApplicationContext());
            checkBoxKarakteristike.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            checkBoxKarakteristike.setGravity(Gravity.RIGHT);
            LinearLayout linLayCheck = new LinearLayout(getApplicationContext());
            linLayCheck.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,0.3f));
            linLayCheck.addView(checkBoxKarakteristike);

            linLayKarakteristika.addView(linLayIme);
            linLayKarakteristika.addView(linLayCheck);

            linearLayoutKarakteristikeMain.addView(linLayKarakteristika);
        }

    }
}
