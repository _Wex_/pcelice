package hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase;

/**
 * Created by Murta on 30.12.2015..
 */
public class Djelatnik {

    private Integer id;
    private String username;
    private String password;
    private boolean aktivan;
    private boolean vlasnik;

    public Djelatnik(String username, String password){
        this(null, username, password, false, false);
    }

    public Djelatnik(Integer id, String username, String password){
        this(id, username, password, false, false);
    }

    public Djelatnik(String username, String password, boolean aktivan, boolean vlasnik){
        this(null, username, password, aktivan, vlasnik);
    }

    public Djelatnik(Integer id, String username, String password, boolean aktivan, boolean vlasnik){
        if (username.length() > 20 || password.length() > 20) {
            //TODO once again
        }
        if(id == null){
            this.id = null;
        }
        else{
            this.id = new Integer(id);
        }
        this.username = username;
        this.password = password;
        this.aktivan = aktivan;
        this.vlasnik = vlasnik;
    }


    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean jeAktivan() {
        return aktivan;
    }

    public boolean jeVlasnik() {
        return vlasnik;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Djelatnik djelatnik = (Djelatnik) o;

        return username.equals(djelatnik.username);

    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }
}
