package hr.fer.opp.pcelice.jelasrostilja.naslovna;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;

import hr.fer.opp.pcelice.jelasrostilja.R;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.PrivremenaBaza;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Jelo;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Narudzba;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.NarudzbaJelo;

public class StatistikaActivity extends AppCompatActivity {

    private LinkedList<Narudzba> sveNarudzbe = new LinkedList<>();
    private LinearLayout linearLayoutMain = null;
    private int godinaMin;
    private int godinaMax;
    private int mjesecMin;
    private int mjesecMax;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistika);
        initEverything();
    }
    private void initEverything(){
        sveNarudzbe = (LinkedList) PrivremenaBaza.baza.vratiSveNarudzbe();
        if(sveNarudzbe.size() == 0){finish();}
        linearLayoutMain = (LinearLayout) findViewById(R.id.linearLayoutStatistika);
        boolean start = true;
        int godina = 0;
        int mjesec = 0;
        for (Narudzba trenutnaNarudzba : sveNarudzbe) {
            if (start) {
                start = false;
                godinaMin = Integer.valueOf(trenutnaNarudzba.getVrijeme().split("[.]")[2]);
                godinaMax = godinaMin;
                mjesecMin = Integer.valueOf(trenutnaNarudzba.getVrijeme().split("[.]")[1]);
                mjesecMax = mjesecMin;
            } else{
                godina = Integer.valueOf(trenutnaNarudzba.getVrijeme().split("[.]")[2]);
                mjesec = Integer.valueOf(trenutnaNarudzba.getVrijeme().split("[.]")[1]);
                if(mjesec<mjesecMin && godina <= godinaMin){mjesecMin = mjesec;}
                else if(mjesec>mjesecMax && godina >= godinaMax){mjesecMax = mjesec;}
                if(godina<godinaMin){godinaMin = godina;}
                else if(godina>godinaMax){godinaMax = godina;}
            }
        }
        boolean nijeGotovo = true;
        LinkedList<Narudzba> narudzbeTrenutnogMjeseca = null;
        for(godina = godinaMin; godina<=godinaMax && nijeGotovo; godina++){
            for(mjesec = mjesecMin; mjesec<=12 && nijeGotovo; mjesec++){
                narudzbeTrenutnogMjeseca = new LinkedList<>();
                int godinaTren = 0;
                int mjesecTren = 0;
                for(Narudzba trenutnaNarudzba : sveNarudzbe){
                    godinaTren = Integer.valueOf(trenutnaNarudzba.getVrijeme().split("[.]")[2]);
                    mjesecTren = Integer.valueOf(trenutnaNarudzba.getVrijeme().split("[.]")[1]);
                    if (mjesecTren==mjesec && godinaTren==godina){
                        narudzbeTrenutnogMjeseca.add(trenutnaNarudzba);
                    }
                }
                PrikazMjeseca noviMjesec = new PrikazMjeseca(godina, mjesec, narudzbeTrenutnogMjeseca);
                listaPrikazaMjeseci.add(noviMjesec);
                if(godina == godinaMax && mjesec == mjesecMax){
                    nijeGotovo = false;
                    break;
                }
            }
        }
    }
    private LinkedList<PrikazMjeseca> listaPrikazaMjeseci = new LinkedList<>();
    private class PrikazMjeseca{
        private int godina = 0;
        private int mjesec = 0;
        private LinkedList<Narudzba> narudzbeOvogMjeseca = new LinkedList<>();
        private LinearLayout linearLayoutPrikazaMjeseciMain = null;
        private double ukupnaCijena = 0;
        private double prosjecnaCijena = 0;
        private int brojNarudzbi = 0;
        private Jelo jeloBR1 = null;
        private int jelo1broj = 0;
        private Jelo jeloBR2 = null;
        private int jelo2broj = 0;
        private Jelo jeloBR3 = null;
        private int jelo3broj = 0;
        PrikazMjeseca(int godina, int mjesec, LinkedList<Narudzba> narudzbeOvogMjeseca){
            if(narudzbeOvogMjeseca.size()==0){
                return;
            }
            this.godina = godina;
            this.mjesec = mjesec;
            this.narudzbeOvogMjeseca = narudzbeOvogMjeseca;
            izracunajCijene();
            najcescaJela();

            linearLayoutPrikazaMjeseciMain = new LinearLayout(getApplicationContext());
            linearLayoutPrikazaMjeseciMain.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            linearLayoutPrikazaMjeseciMain.setOrientation(LinearLayout.VERTICAL);

            TextView textViewDatum = new TextView(getApplicationContext());
            textViewDatum.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textViewDatum.setTextColor(Color.BLACK);
            textViewDatum.setText("MJESEC: " + mjesec + "." + godina);
            textViewDatum.setTextSize(20);
            linearLayoutPrikazaMjeseciMain.addView(textViewDatum);

            TextView textViewBrojNarudzbi = new TextView(getApplicationContext());
            textViewBrojNarudzbi.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textViewBrojNarudzbi.setTextColor(Color.BLACK);
            textViewBrojNarudzbi.setText("BROJ NARUDŽBI: " + brojNarudzbi);
            linearLayoutPrikazaMjeseciMain.addView(textViewBrojNarudzbi);

            TextView textViewProsjekNarudzbi = new TextView(getApplicationContext());
            textViewProsjekNarudzbi.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textViewProsjekNarudzbi.setTextColor(Color.BLACK);
            textViewProsjekNarudzbi.setText("PROSJEČNA CIJENA NARUDŽBA: " + String.format("%.2f",prosjecnaCijena));
            linearLayoutPrikazaMjeseciMain.addView(textViewProsjekNarudzbi);

            TextView textViewBroj1 = new TextView(getApplicationContext());
            textViewBroj1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textViewBroj1.setTextColor(Color.BLACK);
            textViewBroj1.setText("NAJVIŠE NARUČENO JELO: " + jeloBR1.getIme() + "  x" + jelo1broj);
            linearLayoutPrikazaMjeseciMain.addView(textViewBroj1);

            TextView textViewBroj2 = new TextView(getApplicationContext());
            textViewBroj2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textViewBroj2.setTextColor(Color.BLACK);
            if(jeloBR2!= null){textViewBroj2.setText("DRUGO NAJVIŠE NARUČENO JELO: " + jeloBR2.getIme() +"  x" + jelo2broj);}
            else {textViewBroj2.setText("NEMA DRUGOG NAJVIŠE NARUČENOG JELA!");}
            linearLayoutPrikazaMjeseciMain.addView(textViewBroj2);

            TextView textViewBroj3 = new TextView(getApplicationContext());
            textViewBroj3.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textViewBroj3.setTextColor(Color.BLACK);
            if(jeloBR3!= null){textViewBroj3.setText("TREĆE NAJVIŠE NARUČENO JELO: " + jeloBR3.getIme()+"  x" + jelo3broj);}
            else {textViewBroj3.setText("NEMA TREĆEG NAJVIŠE NARUČENOG JELA!");}
            linearLayoutPrikazaMjeseciMain.addView(textViewBroj3);

            TextView textViewUkupno = new TextView(getApplicationContext());
            textViewUkupno.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textViewUkupno.setTextColor(Color.BLACK);
            textViewUkupno.setText("UKUPNA CIJENA NARUDŽBA: " + String.format("%.2f",ukupnaCijena));
            linearLayoutPrikazaMjeseciMain.addView(textViewUkupno);

            LinearLayout linearLayoutLine = new LinearLayout(getApplicationContext());
            linearLayoutLine.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,5));
            linearLayoutLine.setBackgroundColor(Color.BLACK);

            linearLayoutMain.addView(linearLayoutPrikazaMjeseciMain);
            linearLayoutMain.addView(linearLayoutLine);
        }

        private void izracunajCijene(){
            for(Narudzba trenutnaNarudzba : narudzbeOvogMjeseca){
                brojNarudzbi++;
                NarudzbaJelo narudzbaJeloTrenutneNarudzbe[] = PrivremenaBaza.baza.jelaNarudzbe(trenutnaNarudzba.getId());
                int cijenaNarudzbe = 0;
                for(NarudzbaJelo tren : narudzbaJeloTrenutneNarudzbe){
                    cijenaNarudzbe += PrivremenaBaza.baza.vratiJelo(tren.getJeloID()).getCijena();
                }
                ukupnaCijena += cijenaNarudzbe;
            }
            prosjecnaCijena = ukupnaCijena/brojNarudzbi;
        }

        private LinkedList<BrojJela> listaBrojevaJela = new LinkedList<>();
        private void najcescaJela(){
            for(Narudzba trenutnaNarudzba : narudzbeOvogMjeseca){
                NarudzbaJelo narudzbaJeloTrenutneNarudzbe[] = PrivremenaBaza.baza.jelaNarudzbe(trenutnaNarudzba.getId());
                for(NarudzbaJelo trenutnaNarudzbaJelo : narudzbaJeloTrenutneNarudzbe){
                    Jelo trenutnoJelo = PrivremenaBaza.baza.vratiJelo(trenutnaNarudzbaJelo.getJeloID());
                    boolean pronadjenoJelo = false;
                    for(BrojJela trenutniBrojJela : listaBrojevaJela){
                        if(trenutniBrojJela.getJeloKlase().getId() == trenutnoJelo.getId()){
                            pronadjenoJelo = true;
                            trenutniBrojJela.povecaj(trenutnaNarudzbaJelo.getKolicina());
                            break;
                        }
                    }
                    if(pronadjenoJelo == false) {
                        BrojJela novoBrojJela = new BrojJela(trenutnoJelo);
                        novoBrojJela.povecaj(trenutnaNarudzbaJelo.getKolicina());
                        listaBrojevaJela.add(novoBrojJela);
                    }
                }
            }
            BrojJela brojJela = null;
            for(BrojJela trenutniBrojJela : listaBrojevaJela){
                if (trenutniBrojJela.getBrojNarucivanja()>jelo1broj){
                    jelo1broj = trenutniBrojJela.getBrojNarucivanja();
                    jeloBR1 = trenutniBrojJela.getJeloKlase();
                    brojJela = trenutniBrojJela;
                }
            }
            listaBrojevaJela.remove(brojJela);
            try {
                for (BrojJela trenutniBrojJela : listaBrojevaJela) {
                    if (trenutniBrojJela.getBrojNarucivanja() > jelo2broj) {
                        jelo2broj = trenutniBrojJela.getBrojNarucivanja();
                        jeloBR2 = trenutniBrojJela.getJeloKlase();
                        brojJela = trenutniBrojJela;
                    }
                }
                listaBrojevaJela.remove(brojJela);
            } catch (Exception e){}
            try {
                for (BrojJela trenutniBrojJela : listaBrojevaJela) {
                    if (trenutniBrojJela.getBrojNarucivanja() > jelo3broj) {
                        jelo3broj = trenutniBrojJela.getBrojNarucivanja();
                        jeloBR3 = trenutniBrojJela.getJeloKlase();
                        brojJela = trenutniBrojJela;
                    }
                }
                listaBrojevaJela.remove(brojJela);
            } catch (Exception e){}
        }
        private class BrojJela{
            private final Jelo jeloKlase;
            int brojNarucivanja = 0;
            BrojJela(Jelo jeloKlase){
                this.jeloKlase  = jeloKlase;
            }
            public Jelo getJeloKlase(){
                return jeloKlase;
            }
            public void povecaj(){
                brojNarucivanja++;
            }
            public void povecaj(int pov){
                brojNarucivanja += pov;
            }
            public int getBrojNarucivanja(){
                return brojNarucivanja;
            }
        }
    }
}
