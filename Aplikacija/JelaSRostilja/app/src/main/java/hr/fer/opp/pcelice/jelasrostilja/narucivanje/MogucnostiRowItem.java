package hr.fer.opp.pcelice.jelasrostilja.narucivanje;

/**
 * Created by luka on 17.01.16..
 */
public class MogucnostiRowItem {
    public String mogucnost;
    public boolean odabrano;

    public MogucnostiRowItem(String mogucnost, boolean odabrano){
        this.mogucnost = mogucnost;
        this.odabrano = odabrano;
    }

}
