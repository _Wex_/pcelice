package hr.fer.opp.pcelice.jelasrostilja.narucivanje;


import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hr.fer.opp.pcelice.jelasrostilja.R;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.PrivremenaBaza;

public class PrikaznikJeloRowItema extends ArrayAdapter<JeloRowItem> {

    Context context;

    public PrikaznikJeloRowItema(Context context, int resourceId,
                                 List<JeloRowItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView slikaJela;
        ImageButton kosarica;
        TextView naslovJela;
        TextView opisJela;
        ImageView cestoNarucivano;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        JeloRowItem jeloRowItem = getItem(position);
        int brojNarucivanja = jeloRowItem.getJelo().getBrojNarucivanja();

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.jela_list_item, null);
            holder = new ViewHolder();
            holder.kosarica = (ImageButton) convertView.findViewById(R.id.jelaItemKosaricaButton);
            holder.opisJela = (TextView) convertView.findViewById(R.id.upravljanjeJelimaItemDostupno);
            holder.naslovJela = (TextView) convertView.findViewById(R.id.upravljanjeJelimaItemNaslov);
            holder.slikaJela = (ImageView) convertView.findViewById(R.id.upravljanjeJelimaItemSlika);
            holder.cestoNarucivano = (ImageView) convertView.findViewById(R.id.jelaItemCestoNarucivano);

            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.opisJela.setText(jeloRowItem.getOpisJela());
        holder.naslovJela.setText(jeloRowItem.getNaslov());
        try {
            holder.slikaJela.setImageResource(jeloRowItem.getSlikaID());
        } catch (Exception e) {
            try {
                String adresa = PrivremenaBaza.baza.procitajInformaciju("slika" + jeloRowItem.getSlikaID());
                holder.slikaJela.setImageURI(Uri.parse(adresa));

            } catch (Exception ex) {
            }
        }
        if(brojNarucivanja > 2 * PrivremenaBaza.prosjekNarucivanjaJela){
            holder.cestoNarucivano.setImageResource(R.drawable.cestonarucivano);
        }else{
            holder.cestoNarucivano.setImageResource(R.drawable.prazna);
        }

        return convertView;
    }
}




