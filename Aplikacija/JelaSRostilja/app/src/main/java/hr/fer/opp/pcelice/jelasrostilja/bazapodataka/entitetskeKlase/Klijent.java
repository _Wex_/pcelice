package hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase;

/**
 * Created by Murta on 30.12.2015..
 */
public class Klijent {

    private Integer id;
    private String username;
    private String password;

    public Klijent(String username, String password){
        this(null, username, password);
    }

    public Klijent(Integer id, String username, String password){
        if (username.length() > 20 || password.length() > 20) {
            //TODO once again
        }
        if(id == null){
            this.id = null;
        }
        else{
            this.id = new Integer(id);
        }
        this.username = username;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Klijent klijent = (Klijent) o;

        return !(username != null ? !username.equals(klijent.username) : klijent.username != null);

    }

    @Override
    public int hashCode() {
        return username != null ? username.hashCode() : 0;
    }
}
