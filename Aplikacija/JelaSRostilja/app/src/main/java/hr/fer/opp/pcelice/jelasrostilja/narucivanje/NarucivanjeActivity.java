package hr.fer.opp.pcelice.jelasrostilja.narucivanje;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.format.Time;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import hr.fer.opp.pcelice.jelasrostilja.R;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.PrivremenaBaza;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Jelo;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Kategorija;
import hr.fer.opp.pcelice.jelasrostilja.mail.GMailSender;
import hr.fer.opp.pcelice.jelasrostilja.mail.MailThread;

public class NarucivanjeActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    // TODO LUKA: uljepsat prikaz kategorije

    private int mod = 1; //1. kategorije jela 2. jela 3.kosarica 4.potvrdi kupnju 5.mogucnostiRowItems
    Spinner spinner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent arrivingIntent = getIntent();
        mod = arrivingIntent.getIntExtra("mod",1);

        if(mod == 1) {
            pokreniPrikazKategorija();
        }
        if(mod == 3) {
            pokreniPrikazKosarice();
        }
    }

    ListView listViewMogucnosti;
    List<MogucnostiRowItem> mogucnostiRowItems;
    PrikaznikMogucnostiRowItema adapterMogucnosti;
    Jelo kliknutoJeloMogucnosti;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        if(mod == 1) {
            Kategorija kliknuta = kategorijaRowItems.get(position).getKategorija();
            kategorijaJela = kliknuta.getId();
            sveKategorijeJela = false;
            pokreniPrikazJela();
            return;
        }
        if(mod == 2) {
            mod = 5;
            // TODO LUKA: prikaz dodatnih mogucnostiRowItems jela
            Jelo kliknuto = jeloRowItems.get(position).getJelo();
            kliknutoJeloMogucnosti = kliknuto;

            setContentView(R.layout.mogucnosti_jela);
            ImageView slika = ((ImageView) findViewById(R.id.mogucnostiSlikaJela));
            try {
                slika.setImageResource(kliknuto.getSlikaID());
            } catch (Exception e) {
                try {
                    String adresa = PrivremenaBaza.baza.procitajInformaciju("slika" + kliknuto.getSlikaID());
                    slika.setImageURI(Uri.parse(adresa));
                } catch (Exception ex) {
                }
            }
            ((TextView) findViewById(R.id.mogucnostiNazivJela)).setText(kliknuto.getIme());
            ((TextView) findViewById(R.id.mogucnostiOpisJela)).setText(kliknuto.getOpisJela());

            List<String> karakteristike = KarakteristikeKategorije.vratiKarakteristikeJela(kliknuto);
            mogucnostiRowItems = new ArrayList<>();
            for(String kar : karakteristike){
                mogucnostiRowItems.add(new MogucnostiRowItem(kar, false));
            }

            listViewMogucnosti = (ListView) findViewById(R.id.mogucnostiLista);
            adapterMogucnosti = new PrikaznikMogucnostiRowItema(this,
                    R.layout.mogucnosti_list_item, mogucnostiRowItems);
            listViewMogucnosti.setAdapter(adapterMogucnosti);
            listViewMogucnosti.setOnItemClickListener(this);

            return;
        }
        if(mod == 3) {
            Jelo kliknuto = kosaricaRowItems.get(position).getJelo();
            Toast toast = Toast.makeText(getApplicationContext(),
                    kliknuto.getIme() + " - količina: " + Kosarica.kolicinaJela(kliknuto),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
            return;
        }
        if(mod == 5){
            // TODO LUKA: bilo bi zgodno da se i klikom na jelo mjenja odabir ali nije nuzno
        }
    }


    @Override
    public void onBackPressed()
    {
        if(mod == 1) {
            super.onBackPressed();
            finish();
        }
        else if(mod == 2) {
            pokreniPrikazKategorija();
        }
        else if(mod == 3) {
            pokreniPrikazJela();
        }
        else if(mod == 4){
            pokreniPrikazKosarice();
        }
        else if(mod == 5){
            pokreniPrikazJela();
        }
    }


    /*-------------PRIKAZ KATEGORIJA-----------*/

    ListView listViewKategorije;
    List<KategorijaRowItem> kategorijaRowItems;
    public PrikaznikKategorijaRowItema adapterKategorije;

    private void pokreniPrikazKategorija(){
        setContentView(R.layout.activity_prikaz_kategorija);
        mod = 1;
        kategorijaRowItems = new ArrayList<KategorijaRowItem>();
        Kategorija[] kategorije = PrivremenaBaza.baza.vratiSveKategorije();
        Jelo[] jela = PrivremenaBaza.baza.vratiDostupnaJela();
        for(Kategorija kategorija : kategorije){
            for(Jelo jelo : jela){
                if(jelo.getKategorijaID() == kategorija.getId()){
                    if(!kategorijaRowItems.contains(kategorija)) {
                        kategorijaRowItems.add(new KategorijaRowItem(kategorija));
                        break;
                    }
                }
            }
        }
        listViewKategorije = (ListView) findViewById(R.id.kategorijaListaKategorija);
        adapterKategorije = new PrikaznikKategorijaRowItema(this,
                R.layout.kategorija_list_item, kategorijaRowItems);
        listViewKategorije.setAdapter(adapterKategorije);
        listViewKategorije.setOnItemClickListener(this);
    }

    public void stisnutGumbPrikaziSvaJela(View view){
        kategorijaJela = 0;
        sveKategorijeJela = true;
        pokreniPrikazJela();
    }


    /*---------------PRIKAZ JELA---------------*/


    public PrikaznikJeloRowItema adapterJela;
    ListView listViewJela;
    List<JeloRowItem> jeloRowItems;

    private int kategorijaJela = 0;
    private boolean sveKategorijeJela = false;

    private void pokreniPrikazJela(){
        setContentView(R.layout.activity_prikaz_jela);
        Jelo[] dostupnaJela = PrivremenaBaza.baza.vratiDostupnaJela();
        jeloRowItems = new ArrayList<JeloRowItem>();
        mod = 2;
        if(sveKategorijeJela){
            for (Jelo jelo : dostupnaJela) {
                JeloRowItem item = new JeloRowItem(jelo);
                jeloRowItems.add(item);
            }
        }else{
            for (Jelo jelo : dostupnaJela) {
                int id = jelo.getKategorijaID();
                if(jelo.getKategorijaID() == kategorijaJela){
                    JeloRowItem item = new JeloRowItem(jelo);
                    jeloRowItems.add(item);
                }
            }
        }

        listViewJela = (ListView) findViewById(R.id.jelaListaJela);
        adapterJela = new PrikaznikJeloRowItema(this,
                R.layout.jela_list_item, jeloRowItems);
        listViewJela.setAdapter(adapterJela);
        listViewJela.setOnItemClickListener(this);
    }


    public void pritisnutGumbDodaneMogucnosti(View view) {
        List<String> selektiraneKarakteristike = new ArrayList<>();
        for(MogucnostiRowItem mog : mogucnostiRowItems){
            if(mog.odabrano){
                selektiraneKarakteristike.add(mog.mogucnost);
            }
        }
        kliknutoJeloMogucnosti.dodajSveKarakteristike(selektiraneKarakteristike);

        Kosarica.dodajUKosaricu(kliknutoJeloMogucnosti, 1);

        mod = 2;
        pokreniPrikazJela();
    }

    public void stisnutCheckboxMogucnosti(View view) {
        CheckBox cb = (CheckBox) view;
        cb.setSelected(!cb.isSelected());
        View redak = (View) view.getParent();
        ListView listaMog = (ListView) redak.getParent();

        final int position = listaMog.getPositionForView(redak);
        MogucnostiRowItem rowItem = mogucnostiRowItems.get(position);
        rowItem.odabrano = !rowItem.odabrano;

    }


    public void stisnutGumbKosariceZaJelo(View view) {
        View redak = (View) view.getParent();
        ListView listaJ = (ListView) redak.getParent();
        final int position = listaJ.getPositionForView(redak);
        Jelo kliknuto = jeloRowItems.get(position).getJelo();
        Kosarica.dodajUKosaricu(kliknuto, 1);

        Toast toast = Toast.makeText(getApplicationContext(),
                "Jelo \"" + kliknuto.getIme() + "\" dodano u kosaricu.",
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }


    /*------------PRIKAZ KOSARICE---------------*/

    ListView listViewKosarica;
    List<KosaricaRowItem> kosaricaRowItems;
    PrikaznikKosaricaRowItema adapterKosarica;
    NumberFormat formatterKosarica = new DecimalFormat("#0.00 kn");


    public void pokreniPrikazKosarice(View view){pokreniPrikazKosarice();}
    private void pokreniPrikazKosarice(){
        setContentView(R.layout.activity_kosarica);
        mod = 3;
        kosaricaRowItems = new ArrayList<KosaricaRowItem>();
        for(Jelo jelo : Kosarica.sadrzanaJela()){
            kosaricaRowItems.add(new KosaricaRowItem(jelo, Kosarica.kolicinaJela(jelo)));
        }

        listViewKosarica = (ListView) findViewById(R.id.kosaricaListaKosarica);
        adapterKosarica = new PrikaznikKosaricaRowItema(this,
                R.layout.kosarica_list_item, kosaricaRowItems);
        listViewKosarica.setAdapter(adapterKosarica);
        listViewKosarica.setOnItemClickListener(this);

        TextView iznosNarudzbeTextView = (TextView) findViewById(R.id.kosaricaIznosNarudzbeTextView);
        iznosNarudzbeTextView.setText(formatterKosarica.format(Kosarica.vrijednostKosarice(0)));
    }


    public void stisnutGumbPotvrdiKupnju(View view){

        prikazPotvrdiKupnju();
    }

    public void povecajKolicinuJela(View view){
        View redak = (View) view.getParent();
        ListView listaJ = (ListView) redak.getParent();
        final int position = listaJ.getPositionForView(redak);
        KosaricaRowItem rowItem = kosaricaRowItems.get(position);

        Jelo kliknuto = rowItem.getJelo();
        Kosarica.dodajUKosaricu(kliknuto, 1);
        osvjeziStanjePrikazaKosarice();

        Toast toast = Toast.makeText(getApplicationContext(),
                "Jelo \"" + kliknuto.getIme() + "\" dodano u kosaricu.",
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    public void smanjiKolicinuJela(View view){
        View redak = (View) view.getParent();
        ListView listaJ = (ListView) redak.getParent();

        final int position = listaJ.getPositionForView(redak);
        KosaricaRowItem rowItem = kosaricaRowItems.get(position);

        Jelo kliknuto = rowItem.getJelo();
        Kosarica.makniIzKosarice(kliknuto, 1);

        osvjeziStanjePrikazaKosarice();

        Toast toast = Toast.makeText(getApplicationContext(),
                "Smanjena kolicina jela \"" + kliknuto.getIme(),
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();

    }

    public void makniJelo(View view){
        View redak = (View) view.getParent();
        ListView listaJ = (ListView) redak.getParent();

        final int position = listaJ.getPositionForView(redak);
        KosaricaRowItem rowItem = kosaricaRowItems.get(position);

        Jelo kliknuto = rowItem.getJelo();
        Kosarica.makniIzKosarice(kliknuto, Kosarica.kolicinaJela(kliknuto));

        osvjeziStanjePrikazaKosarice();

        Toast toast = Toast.makeText(getApplicationContext(),
                "Jelo \"" + kliknuto.getIme() + "\" maknuto iz kosarice.",
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    private void osvjeziStanjePrikazaKosarice(){
        kosaricaRowItems = new ArrayList<KosaricaRowItem>();

        for(Jelo jelo : Kosarica.sadrzanaJela()){
            kosaricaRowItems.add(new KosaricaRowItem(jelo, Kosarica.kolicinaJela(jelo)));
        }

        adapterKosarica.clear();
        adapterKosarica.addAll(kosaricaRowItems);
        adapterKosarica.notifyDataSetChanged();

        TextView iznosNarudzbeTextView = (TextView) findViewById(R.id.kosaricaIznosNarudzbeTextView);
        iznosNarudzbeTextView.setText(formatterKosarica.format(Kosarica.vrijednostKosarice(0)));
    }






    /*--------------PRIKAZ POTVRDI KUPNJU------------*/
    DecimalFormat formatter = new DecimalFormat("#.00 kn");

    boolean placanjeGotovinom = true;

    private void prikazPotvrdiKupnju(){

        mod = 4;
        if(Kosarica.vrijednostKosarice(0) < PrivremenaBaza.minimalnaNarudzba){
            setContentView(R.layout.potvrda_premala_narudzba_error);
        }else{
            setContentView(R.layout.activity_potvrdi_kupnju);
            spinner = (Spinner) findViewById(R.id.potvrdaIzbornikNacinaPlacanja);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int pos, long id) {

                    TextView cijenaNarudzbe = (TextView) findViewById(R.id.potvrdaCijenaNarudzbe);
                    String nacinPlacanja = parent.getItemAtPosition(pos).toString();
                    if (nacinPlacanja.contains("Gotovina")) {
                        placanjeGotovinom = true;
                        ((TextView) findViewById(R.id.potvrdaPorukaKartica)).setVisibility(View.GONE);
                        ((TextView) findViewById(R.id.potvrdaBrojKarticeEditText)).setVisibility(View.GONE);
                        cijenaNarudzbe.setText(formatter.format(Kosarica.vrijednostKosarice(0.1)));
                    } else {
                        placanjeGotovinom = false;
                        ((TextView) findViewById(R.id.potvrdaPorukaKartica)).setVisibility(View.VISIBLE);
                        ((TextView) findViewById(R.id.potvrdaBrojKarticeEditText)).setVisibility(View.VISIBLE);
                        cijenaNarudzbe.setText(formatter.format(Kosarica.vrijednostKosarice(0)));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // Another interface callback
                }

            });
            TextView cijenaNarudzbe = (TextView) findViewById(R.id.potvrdaCijenaNarudzbe);
            cijenaNarudzbe.setText(formatter.format(Kosarica.vrijednostKosarice(0.1)));
        }
    }

    public void provediNarudzbu(View view){
        EditText email = (EditText) findViewById(R.id.potvrdaEmailEditText);
        EditText mobitel = (EditText) findViewById(R.id.potvrdaMobitelEditText);
        EditText adresa = (EditText) findViewById(R.id.potvrdaAdresaEditText);
        EditText kat = (EditText) findViewById(R.id.potvrdaKatEditText);
        EditText brojKartice = (EditText) findViewById(R.id.potvrdaBrojKarticeEditText);

        if(TextUtils.isEmpty(adresa.getText().toString())) {
            adresa.setError("Molimo upisite svoju adresu");
            return;
        }else{
            if(TextUtils.isEmpty(kat.getText().toString())) {
                kat.setError("Molimo upisite svoj kat");
                return;
            }else{
                if(TextUtils.isEmpty(mobitel.getText().toString())) {
                    mobitel.setError("Molimo upisite svoj broj mobitela");
                    return;
                }else{
                    if(TextUtils.isEmpty(email.getText().toString())) {
                        email.setError("Molimo upisite svoju e-mail adresu");
                        return;
                    }else{
                        if(!placanjeGotovinom){
                            String brojKar = brojKartice.getText().toString();
                            if(brojKar.isEmpty()){
                                brojKartice.setError("Molimo upisite broj kartice");
                                return;
                            }
                            int sum = 0;
                            boolean alternate = false;
                            for (int i = brojKar.length() - 1; i >= 0; i--)
                            {
                                int n = Integer.parseInt(brojKar.substring(i, i + 1));
                                if (alternate)
                                {
                                    n *= 2;
                                    if (n > 9)
                                    {
                                        n = (n % 10) + 1;
                                    }
                                }
                                sum += n;
                                alternate = !alternate;
                            }
                            boolean valjan = (sum % 10 == 0);
                            if(!valjan){
                                brojKartice.setError("Broj kartice nije ispravan!");
                                return;
                            }else{
                                // TODO LUKA: ovdje treba naplatit karticu no kad je cijeli restoran lazan onda je i naplata lazna
                                // #aintcare
                            }
                        }
                    }
                }
            }
        }


        int idNarudzbe = PrivremenaBaza.baza.ubaciNarudzbu(PrivremenaBaza.baza.ubaciPodatke(mobitel.getText().toString(), adresa.getText().toString(),
                email.getText().toString(), Integer.parseInt(kat.getText().toString())));

        for(Jelo jelo : Kosarica.sadrzanaJela()){
            PrivremenaBaza.baza.narudzbaJelo(idNarudzbe, jelo.getId(), Kosarica.kolicinaJela(jelo), jelo.getKarakteristike());
        }
        PrivremenaBaza.osvjeziProsjekNarucivanjaJela();

        setContentView(R.layout.activity_potvrda_uspjeh);

        // TODO LUKA: vidjeti sta je s ovim, ovog ne bi trebalo bit ali mail se ne salje ako maknem
        /*StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);*/

        // SLANJE MAILA KORISNIKU---------------------------------------
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();


        String tekstNarudzbe = "Poštovani, \n"
                + "dana " + today.format("%d.%m.%Y.") + " u " + today.format("%H:%M") + " sati "
                + "ste naručili sljedeća jela:\n";

        for(Jelo jelo : Kosarica.sadrzanaJela()){
            tekstNarudzbe += " - " + jelo.toString() + " - količina: " + Kosarica.kolicinaJela(jelo) + "\n";
            for(String karakteristika : jelo.getKarakteristike()){
                if(!karakteristika.equals("")){
                    tekstNarudzbe += "     + " + karakteristika + "\n";
                }
            }
        }

        if(placanjeGotovinom){
            tekstNarudzbe += "Ukupna vrijednost narudžbe je " + formatterKosarica.format(Kosarica.vrijednostKosarice(0.1)) + "\n";
            tekstNarudzbe += "Ostvaren je popust od 10% na placanje gotovinom.\n";
        }else{
            tekstNarudzbe += "Ukupna vrijednost narudžbe je " + formatterKosarica.format(Kosarica.vrijednostKosarice(0)) + "\n";
        }

        tekstNarudzbe += "Naručena jela uskoro stižu na Vašu adresu: " + adresa.getText().toString() + "\n" +
                "Zahvaljujemo Vam na ukazanom povjerenju.\n" +
                "Vaš Restoran Wild8.";

        Thread slanjeMaila = new Thread(new MailThread("Wild8 Restoran narudzba", tekstNarudzbe, email.getText().toString()));
        slanjeMaila.start();

        Kosarica.checkout();

    }



    public void nastaviKupnju(View view){
        finish();
    }

    public void vratiSeNaKosaricu(View view){
        pokreniPrikazKosarice();
    }
}
