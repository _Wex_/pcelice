package hr.fer.opp.pcelice.jelasrostilja.narucivanje;

import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Kategorija;

public class KategorijaRowItem {
    private Kategorija kategorija;


    public KategorijaRowItem(Kategorija kategorija) {
        this.kategorija = kategorija;
    }

    public Kategorija getKategorija() { return kategorija; }


    @Override
    public String toString() {
        return kategorija.toString();
    }
}