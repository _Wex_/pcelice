package hr.fer.opp.pcelice.jelasrostilja.narucivanje;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import hr.fer.opp.pcelice.jelasrostilja.R;

public class PrikaznikKategorijaRowItema extends ArrayAdapter<KategorijaRowItem> {

    Context context;

    public PrikaznikKategorijaRowItema(Context context, int resourceId,
                                       List<KategorijaRowItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        TextView ime;
        TextView opis;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        KategorijaRowItem kategorijaRowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.kategorija_list_item, null);
            holder = new ViewHolder();
            holder.opis = (TextView) convertView.findViewById(R.id.kategorijaItemOpisKategorije);
            holder.ime = (TextView) convertView.findViewById(R.id.kategorijaItemImeKategorije);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.opis.setText(kategorijaRowItem.getKategorija().getOpis());
        holder.ime.setText(kategorijaRowItem.getKategorija().getIme());

        return convertView;
    }
}