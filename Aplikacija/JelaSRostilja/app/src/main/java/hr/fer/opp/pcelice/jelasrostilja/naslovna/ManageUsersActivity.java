package hr.fer.opp.pcelice.jelasrostilja.naslovna;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.Set;

import hr.fer.opp.pcelice.jelasrostilja.R;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.PrivremenaBaza;

public class ManageUsersActivity extends AppCompatActivity {
    private LinearLayout linearLayoutMainAMU_Username = null;
    private LinearLayout linearlayoutMainAMU_Button = null;
    public Boolean vlasnik = false;
    private String trenutniKorisnik;
    private class UserDisplay{
        final private UserDisplay trenutniUserDisplay;
        private TextView textViewUserDisplayName = null;
        private Button buttonUserDisplayDelete = null;
        public UserDisplay(int id ,String username, final Boolean aktivan){
            trenutniUserDisplay = this;

            textViewUserDisplayName = new TextView(getApplicationContext());
            buttonUserDisplayDelete = new Button(getApplicationContext());

            textViewUserDisplayName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            buttonUserDisplayDelete.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            textViewUserDisplayName.setId(id * 6 + 1);
            buttonUserDisplayDelete.setId(id * 6 + 4);
            textViewUserDisplayName.setText(username);
            buttonUserDisplayDelete.setEnabled(vlasnik);
            ((LinearLayout.LayoutParams)textViewUserDisplayName.getLayoutParams()).setMargins(20, 55, 10, 0);
            ((LinearLayout.LayoutParams)buttonUserDisplayDelete.getLayoutParams()).setMargins(0, 10, 0, 0);
            textViewUserDisplayName.setTextColor(Color.BLACK);
            textViewUserDisplayName.setTextSize(20);

            if(aktivan){
                buttonUserDisplayDelete.setText("Deaktiviraj");
                textViewUserDisplayName.setTextColor(Color.BLACK);
            } else {
                buttonUserDisplayDelete.setText("Aktiviraj");
                textViewUserDisplayName.setTextColor(Color.LTGRAY);
            }

            buttonUserDisplayDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(PrivremenaBaza.baza.aktivan(textViewUserDisplayName.getText().toString())){
                        if(textViewUserDisplayName.getText().toString().equals(trenutniKorisnik)){
                            potvrdiRadnju(trenutniUserDisplay);
                        } else {
                            buttonUserDisplayDelete.setText("Aktiviraj");
                            PrivremenaBaza.baza.deaktivirajDjelatnika(textViewUserDisplayName.getText().toString());
                            textViewUserDisplayName.setTextColor(Color.LTGRAY);
                        }
                    } else {
                        buttonUserDisplayDelete.setText("Deaktiviraj");
                        PrivremenaBaza.baza.aktivirajDjelatnika(textViewUserDisplayName.getText().toString());
                        textViewUserDisplayName.setTextColor(Color.BLACK);
                    }
                }
            });

            linearLayoutMainAMU_Username.addView(textViewUserDisplayName);
            linearlayoutMainAMU_Button.addView(buttonUserDisplayDelete);
        }
    }

    private Set<String> usernames = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_users);
        usernames = PrivremenaBaza.baza.sviUsernameovi();
        linearLayoutMainAMU_Username = (LinearLayout)findViewById(R.id.linearLayoutMainAMU_Username);
        linearlayoutMainAMU_Button = (LinearLayout)findViewById(R.id.linearLayoutMainAMU_Button);
        Intent arrivingIntent = getIntent();
        this.vlasnik = arrivingIntent.getBooleanExtra("vlasnik",false);
        this.trenutniKorisnik = arrivingIntent.getStringExtra("korisnik");
        populateList();
    }
    private static int id = 0;
    private LinkedList<UserDisplay> userDisplays = new LinkedList<>();
    private void populateList(){
        for(String ime : usernames){
            UserDisplay newUserDisplay = new UserDisplay(id, ime, PrivremenaBaza.baza.aktivan(ime));
            userDisplays.add(newUserDisplay);
            id++;
        }
    }



    public void newUser(View v){
        noviKorisnik();
    }


    private PopupWindow newUserPopup = null;
    private View newUserView = null;
    private EditText editTextUsernameNPW = null;
    private EditText editTextPasswordNPW = null;
    private EditText editTextConfirmPasswordNPW = null;
    private TextView textViewVlasnikNPW = null;
    private CheckBox checkBoxVlasnikNPW = null;
    private Button buttonNaprijedNPW = null;
    private Button buttonNazadNPW = null;
    private View.OnClickListener onClickListenerNPW = null;
    private void noviKorisnik() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        newUserView = inflater.inflate(R.layout.newuser_popup_window, (ViewGroup) findViewById(R.id.linearLayoutNewUserPopup));
        newUserPopup = new PopupWindow(newUserView, 100, 100, true);
        newUserPopup.showAtLocation(this.findViewById(R.id.linearLayoutAMU), Gravity.CENTER, 0, 0);
        newUserView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        newUserPopup.update(findViewById(R.id.linearLayoutAMU).getWidth(), newUserView.findViewById(R.id.linearLayoutNewUserPopup).getMeasuredHeight());
        newUserPopup.getContentView().setFocusableInTouchMode(true);
        newUserPopup.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    newUserPopup.dismiss();
                    return true;
                }
                return false;
            }
        });
        editTextUsernameNPW = (EditText)newUserView.findViewById(R.id.editTextUsernameNPW);
        editTextPasswordNPW = (EditText)newUserView.findViewById(R.id.editTextPasswordNPW);
        editTextConfirmPasswordNPW = (EditText)newUserView.findViewById(R.id.editTextConfPasswordNPW);
        textViewVlasnikNPW = (TextView)newUserView.findViewById(R.id.textViewVlasnikNPW);
        checkBoxVlasnikNPW = (CheckBox)newUserView.findViewById(R.id.checkboxVlasnikNPW);
        if (vlasnik == false){
            textViewVlasnikNPW.setEnabled(false);
            checkBoxVlasnikNPW.setEnabled(false);
        }
        buttonNaprijedNPW = (Button)newUserView.findViewById(R.id.buttonNaprijedNPW);
        buttonNazadNPW = (Button)newUserView.findViewById(R.id.buttonNazadNPW);
        onClickListenerNPW = getOnClickListenerNPW();
        buttonNaprijedNPW.setOnClickListener(onClickListenerNPW);
        buttonNazadNPW.setOnClickListener(onClickListenerNPW);
    }

    private String errorString;
    private View.OnClickListener getOnClickListenerNPW(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId() == buttonNaprijedNPW.getId()){
                    if(checkNewUserData()){
                        createUser();
                    } else {
                        prijavaGreske(errorString);
                    }
                }
                else if(v.getId() == buttonNazadNPW.getId()){
                    newUserPopup.dismiss();
                }
            }
        };
    }

    private Boolean checkNewUserData(){
        Boolean dobro = true;
        errorString = "";
        if(editTextUsernameNPW.getText().toString().length()>20){
            dobro = false;
            errorString = "Korisničko ime duže od 20 znakova\n";
        }
        else if(editTextUsernameNPW.getText().toString().length()<1){
            dobro = false;
            errorString = "Korisničko ime kraće od 1 znak\n";
        }
        if(editTextPasswordNPW.getText().toString().length()>20){
            dobro = false;
            errorString += "Lozinka duža od 20 znakova\n";
        }
        else if(editTextPasswordNPW.getText().toString().length()<1){
            dobro = false;
            errorString += "Lozinka kraća od 1 znak\n";
        }
        if(!editTextPasswordNPW.getText().toString().equals(editTextConfirmPasswordNPW.getText().toString())){
            dobro = false;
            errorString += "Lozinke nisu jednake\n";
        }
        if(PrivremenaBaza.baza.usernamePostojiUBazi(editTextUsernameNPW.getText().toString())){
            dobro = false;
            errorString += "Korisničko ime je već iskorišteno.\n";
        }
        return dobro;
    }

    private void createUser(){
        PrivremenaBaza.baza.ubaciDjelatnika(editTextUsernameNPW.getText().toString(), editTextPasswordNPW.getText().toString(), checkBoxVlasnikNPW.isChecked());
        newUserPopup.dismiss();
        UserDisplay newUserDisplay = new UserDisplay(id++, editTextUsernameNPW.getText().toString(), true);
        userDisplays.add(newUserDisplay);
    }



    private PopupWindow errorPopup = null;
    private View errorPopupView = null;
    private TextView textViewError = null;
    private Button buttonBack = null;
    private void prijavaGreske(String greska){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        errorPopupView = inflater.inflate(R.layout.error_popup_window,(ViewGroup)findViewById(R.id.linearLayoutErrorPopup));
        errorPopup = new PopupWindow(errorPopupView, 100, 100, true);
        errorPopup.showAtLocation(this.findViewById(R.id.linearLayoutAMU), Gravity.CENTER, 0, 0);
        errorPopupView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        errorPopup.update(findViewById(R.id.linearLayoutAMU).getWidth(), errorPopupView.findViewById(R.id.linearLayoutErrorPopup).getMeasuredHeight());
        errorPopup.getContentView().setFocusableInTouchMode(true);
        errorPopup.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    errorPopup.dismiss();
                    return true;
                }
                return false;
            }
        });
        textViewError = (TextView)errorPopupView.findViewById(R.id.textViewErrorEPW);
        textViewError.setText(greska);
        buttonBack = (Button)errorPopupView.findViewById(R.id.buttonBackEPW);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorPopup.dismiss();
            }
        });
    }

    private PopupWindow confirmPopup = null;
    private View confirmPopupView = null;
    private Button buttonCancel = null;
    private Button buttonAccept = null;
    private void potvrdiRadnju(final UserDisplay trenutniUserDisplay){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        confirmPopupView = inflater.inflate(R.layout.confirm_popup_window, (ViewGroup)findViewById(R.id.linearLayoutMainCPW));
        confirmPopup = new PopupWindow(confirmPopupView, 100, 100, true);
        confirmPopup.showAtLocation(this.findViewById(R.id.linearLayoutAMU), Gravity.CENTER, 0, 0);
        confirmPopupView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        confirmPopup.update(findViewById(R.id.linearLayoutAMU).getWidth(), confirmPopupView.findViewById(R.id.linearLayoutMainCPW).getMeasuredHeight());
        buttonCancel = (Button) confirmPopupView.findViewById(R.id.buttonNE_CPW);
        buttonAccept = (Button) confirmPopupView.findViewById(R.id.buttonDA_CPW);
        View.OnClickListener confirmOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId() == buttonCancel.getId()){
                    confirmPopup.dismiss();
                }
                else if(v.getId() == buttonAccept.getId()){
                    trenutniUserDisplay.buttonUserDisplayDelete.setText("Aktiviraj");
                    PrivremenaBaza.baza.deaktivirajDjelatnika(trenutniUserDisplay.textViewUserDisplayName.getText().toString());
                    trenutniUserDisplay.textViewUserDisplayName.setTextColor(Color.LTGRAY);
                    confirmPopup.dismiss();
                }
            }
        };
        buttonCancel.setOnClickListener(confirmOnClickListener);
        buttonAccept.setOnClickListener(confirmOnClickListener);
    }
}
