package hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Murta on 30.12.2015..
 */
public class NarudzbaJelo {

    private Integer id;
    private int narudzbaID;
    private int jeloID;
    private int kolicina;
    private List<String> karakteristike;

    public NarudzbaJelo(int narudzbaID, int jeloID, int kolicina, List<String> karakteristike){
        this(null, narudzbaID, jeloID, kolicina, karakteristike);
    }

    public NarudzbaJelo(Integer id, int narudzbaID, int jeloID, int kolicina, List<String> karakteristike){
        if(kolicina < 0){
            //TODO once again
        }
        if(id == null){
            this.id = null;
        }
        else{
            this.id = new Integer(id);
        }
        this.narudzbaID = narudzbaID;
        this.jeloID = jeloID;
        this.kolicina = kolicina;
        this.karakteristike = new LinkedList<String>();
        this.karakteristike.addAll(karakteristike);
    }

    public int getNarudzbaID() {
        return narudzbaID;
    }

    public int getJeloID() {
        return jeloID;
    }

    public int getKolicina() {
        return kolicina;
    }

    public Integer getId() {
        return id;
    }

    public List<String> getKarakteristike() {
        return karakteristike;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NarudzbaJelo that = (NarudzbaJelo) o;

        if (narudzbaID != that.narudzbaID) return false;
        return jeloID == that.jeloID;

    }

    @Override
    public int hashCode() {
        int result = narudzbaID;
        result = 31 * result + jeloID;
        return result;
    }
}
