package hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase;

/**
 * Created by Murta on 30.12.2015..
 */
public class Recenzija {

    private Integer id;
    private Integer ocjena;
    private String sadrzaj;
    private String username;
    private String datum;

    public Recenzija(String sadrzaj, Integer ocjena, String username, String datum){
        this(null, ocjena, sadrzaj, username, datum);
    }

    public Recenzija(String sadrzaj, String username, String datum) {
        this(null, null, sadrzaj, username, datum);
    }

    public Recenzija(Integer id, String sadrzaj, String username, String datum){
        this(id, null, sadrzaj, username, datum);
    }

    public Recenzija(Integer id, Integer ocjena, String sadrzaj, String username, String datum){
        if(id == null){
            this.id = null;
        }
        else{
            this.id = new Integer(id);
        }
        if(ocjena < 1 || ocjena > 5){
            throw new IllegalArgumentException("Ocjena mora biti 1-5");
        }
        if(sadrzaj.length() > 400){
            //TODO once again
        }
        if(ocjena == null){
            this.ocjena = null;
        }
        else{
            this.ocjena = new Integer(ocjena);
        }
        this.sadrzaj = sadrzaj;
        this.username = username;
        this.datum = datum;
    }

    public Integer getId() {
        return id;
    }

    public Integer getOcjena() {
        return ocjena;
    }

    public String getSadrzaj() {
        return sadrzaj;
    }

    public String getUsername() {
        return username;
    }

    public String getDatum() {
        return datum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Recenzija recenzija = (Recenzija) o;

        return !(id != null ? !id.equals(recenzija.id) : recenzija.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
