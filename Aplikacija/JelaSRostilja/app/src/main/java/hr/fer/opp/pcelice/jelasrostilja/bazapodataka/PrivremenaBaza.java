package hr.fer.opp.pcelice.jelasrostilja.bazapodataka;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import hr.fer.opp.pcelice.jelasrostilja.R;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Jelo;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Kategorija;
import hr.fer.opp.pcelice.jelasrostilja.narucivanje.KarakteristikeKategorije;

/**
 * Created by luka on 07.01.16..
 */
public class PrivremenaBaza {

    public static DBclass baza;
    public static double prosjekNarucivanjaJela = 0;
    public final static double minimalnaNarudzba = 35;

    public static void inicijalizirajBazu(Context c){
        baza = new DBclass(c);
        baza.open();

        baza.ubaciDjelatnika("luka", "papez", false);
        baza.ubaciDjelatnika("Vlasnik", "Vlasnik", true);
        baza.ubaciDjelatnika("VlasnikREZ", "Vlasnik", true);

        baza.spremiInformaciju("imeRestoranaDefault", "Restoran Wild8");
        baza.spremiInformaciju("adresaRestoranaDefault","Unska 3, 10000 Zagreb");
        baza.spremiInformaciju("opisRestoranaDefault", "Restoran je super!");
        baza.spremiInformaciju("vlasnikRestoranaDefault", "Pero Perić");
        baza.spremiInformaciju("radnoVrijemeRestoranaDefault", "07 - 15 h");
        baza.spremiInformaciju("telefonDefault", "+385 00 123 4567");

        for(int i = 0; i < imenaKat.length; i++){
            baza.ubaciKategoriju(imenaKat[i], "luka", opisiKat[i], 0);
        }

        Kategorija[] kategorije = baza.vratiSveKategorije();

        for(Kategorija kategorija : kategorije){
            List<String> sf = new ArrayList<>();
            sf.add("Velika porcija"); sf.add("Mala porcija"); sf.add("Bez mesa");
            KarakteristikeKategorije.ubaciKarakteristikeKategorije(kategorija, sf);
        }

        for(int j = 0; j < imenaKat.length; j++){
            for(int i = 0; i < imena[j].length; i++){
                String kars[] = new String[KarakteristikeKategorije.vratiKarakteristikeKategorije(j+1).size()];
                KarakteristikeKategorije.vratiKarakteristikeKategorije(j+1).toArray(kars);
                baza.ubaciJelo(imena[j][i], opisi[j][i], cijene[j][i], imenaKat[j], true,
                        slike[j][i], "luka", 0,  kars);
            }
        }
    }

    public static void osvjeziProsjekNarucivanjaJela(){
        Jelo[] svaJela = baza.vratiSvaJela();
        int ukupno = 0;
        for(Jelo jelo : svaJela){
            ukupno += jelo.getBrojNarucivanja();
        }
        prosjekNarucivanjaJela = (double) ukupno / (double) svaJela.length;
    }

    public static final String[] imenaKat = new String[] {
            "Američka hrana",
            "Bosanski specijaliteti",
            "Povrće",
            "Prilozi",
            "Pića"
    };

    public static final String[] opisiKat = new String[] {
            "Govedina. Sveta krava američke kuhinje",
            "Najpopularnija jela Balkana",
            "Ukusno povrće, skoro kao meso!",
            "Ajvar, kajmak, majoneza, ketchup i još mnogo toga!",
            "Ugasite žeđ bogatim asortimanom pića!"
    };

    public static final String[][] imena = new String[][] {
            { "Hamburger", "Cheeseburger", "Hot-dog", "Rebarca", "Dimljeni losos"},
            { "Ćevapćići", "Kebab", "Janjetina pod pekom", "Pečena piletina"},
            { "Pržene parprike", "Blitva", "Pomfrit"},
            { "Kečap", "Majoneza", "Ajvar", "Kajmak"},
            { "Cola", "Fanta", "Sprite", "Voda"}
    };

    public static final Double[][] cijene = new Double[][]{
            { 16.0, 17.0, 12.0, 20.0, 30.0 },
            { 23.0, 17.0, 35.0, 25.0 },
            { 10.0, 10.0, 12.0 },
            { 2.0, 2.0, 4.0, 5.0 },
            { 10.0, 10.0, 10.0, 8.0 }
    };

    public static final Integer[][] slike = new Integer[][]{
            {R.drawable.hamburger, R.drawable.cheeseburger, R.drawable.hotdog, R.drawable.rebarca, R.drawable.losos},
            {R.drawable.cevapi, R.drawable.kebab, R.drawable.janjetina, R.drawable.piletina},
            {R.drawable.paprike, R.drawable.blitva, R.drawable.pomfrit},
            {R.drawable.ketchup, R.drawable.majoneza, R.drawable.ajvar, R.drawable.kajmak},
            {R.drawable.cola, R.drawable.fanta, R.drawable.sprite, R.drawable.voda}
            };


    public static final String[][] opisi = new String[][] {
            { "Najfiniji hamburger u gradu.", "Hrpetina sira.", "U domaćem pecivu.", "Mljac - mljac", "Iz kalifornijskih potoka."},
            { "Svačiji favorit.", "Ljuti, blagi? Fini.", "Preporuča se jedenje prstima.", "Dijetna hrana."},
            { "Paše uz svako jelo.", "Dalmatinski stil.", "Svačiji favorit."},
            { "Nije domaći.", "Domaća.", "Domaći.", "Domaći."},
            { "Coca cola", "Od naranče.", "Osvježava.", "Voda u boci."}
           };





}
