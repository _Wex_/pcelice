package hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase;

/**
 * Created by Murta on 30.12.2015..
 */
public class Narudzba {

    private Integer id;
    private int podaciID;
    private boolean preuzeta;
    private boolean spremna;
    private boolean odnesena;
    private boolean isporucena;
    private String vrijeme;
    private String djelatnik;

    public Narudzba(int podaciID, String vrijeme, String djelatnik){
        this(null, podaciID, false, false, false, false, vrijeme, djelatnik);
    }

    public Narudzba(Integer id, int podaciID, String vrijeme, String djelatnik){
        this(id, podaciID, false, false, false, false, vrijeme, djelatnik);
    }

    public Narudzba(int podaciID, boolean preuzeta, boolean spremna, boolean odnesena, boolean isporucena, String vrijeme, String djelatnik){
        this(null, podaciID, preuzeta, spremna, odnesena, isporucena, vrijeme, djelatnik);
    }

    public Narudzba(Integer id, int podaciID, boolean preuzeta, boolean spremna, boolean odnesena, boolean isporucena, String vrijeme, String djelatnik){
        if(id == null){
            this.id = null;
        }
        else{
            this.id = new Integer(id);
        }
        this.podaciID = podaciID;
        this.preuzeta = preuzeta;
        this.spremna = spremna;
        this.odnesena = odnesena;
        this.isporucena = isporucena;
        this.djelatnik = djelatnik;
        this.vrijeme = vrijeme;
    }

    public Integer getId() {
        return id;
    }

    public int getPodaciID() {
        return podaciID;
    }

    public boolean jePreuzeta() {
        return preuzeta;
    }

    public boolean jeSpremna() {
        return spremna;
    }

    public boolean jeOdnesena() {
        return odnesena;
    }

    public boolean jeIsporucena() {
        return isporucena;
    }

    public String getVrijeme() {
        return vrijeme;
    }

    public String getDjelatnik() {
        return djelatnik;
    }

    public boolean isPreuzeta() {
        return preuzeta;
    }

    public boolean isSpremna() {
        return spremna;
    }

    public boolean isOdnesena() {
        return odnesena;
    }

    public boolean isporucena() {
        return isporucena;
    }

    public void preuzmi(){
        if(!preuzeta){
            preuzeta = true;
        }
    }

    public void spremna(){
        if(!spremna){
            spremna = true;
        }
    }

    public void odnesi(){
        if(!odnesena){
            odnesena = true;
        }
    }

    public void isporuci(){
        if(!isporucena){
            isporucena = true;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Narudzba narudzba = (Narudzba) o;

        return !(id != null ? !id.equals(narudzba.id) : narudzba.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
