package hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase;

/**
 * Created by Murta on 30.12.2015..
 */
public class Podaci {

    private Integer id;
    //private int klijentID;
    private String telefon;
    private String adresa;
    private String email;
    private int kat;

    public Podaci(String telefon, String adresa, String email, int kat){
        this(null, telefon, adresa, email, kat);
    }

    public Podaci(Integer id, String telefon, String adresa, String email, int kat){
        if(telefon.length()>20 || adresa.length() > 50){
        }
        if(id == null){
            this.id = null;
        }
        else{
            this.id = new Integer(id);
        }
        //this.klijentID = klijentID;
        this.telefon = telefon;
        this.adresa = adresa;
        this.email = email;
        this.kat = kat;
    }

    public Integer getId() {
        return id;
    }

    public String getTelefon() {
        return telefon;
    }

    public String getAdresa() {
        return adresa;
    }

    public String getEmail() {
        return email;
    }

    public int getKat() {
        return kat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Podaci podaci = (Podaci) o;

        return !(id != null ? !id.equals(podaci.id) : podaci.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
