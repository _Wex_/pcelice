package hr.fer.opp.pcelice.jelasrostilja.bazapodataka;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Djelatnik;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Jelo;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Kategorija;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Klijent;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Narudzba;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.NarudzbaJelo;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Podaci;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Recenzija;


/**
 * Created by Murta on 29.12.2015..
 */
public class DBclass {

    private final Context context;
    private DatabaseOperations dbops;
    private SQLiteDatabase db;

    public DBclass(Context context){
        this.context = context;
        this.dbops = new DatabaseOperations(context);
    }

    public DBclass open(){
        db = dbops.getWritableDatabase();
       // db.delete("jelo", null, null);
        return this;
    }

    public void close(){
        dbops.close();
    }

    private void odradiInsert(ContentValues cv, String tablica){
        long k = db.insert(tablica, null, cv);
        Log.d("DBclass", "One row inserted to " + tablica + " with id " + k + ". Content values: " + cv.toString());
    }

    private int odradiInsertSPovratkom(ContentValues cv, String tablica){
        long k = db.insert(tablica, null, cv);
        Log.d("DBclass", "One row inserted to " + tablica + " with id " + k + ". Content values: " + cv.toString());
        return Integer.parseInt(new String(""+k));
    }

    private void ubaciKlijenta(String username, String password){
        if(username.length()>20 || password.length() > 20){
            //ovo biste vi trebali provjeriti u aplikaciji
            //baza nece ubaciti nista, ali ona ne bi trebala biti glavni provjeritelj
        }
        ContentValues cv = new ContentValues();
        cv.put("username", username);
        cv.put("password", password);
        odradiInsert(cv, "klijent");
    }

    private Klijent uhvatiKlijentaPoID(int id){
        String[] columns = {"id", "username", "password"};
        Cursor cursor = db.query("klijent", columns, "id = " + id, null, null, null, null);
        cursor.moveToFirst();
        return new Klijent(new Integer(cursor.getString(0)), cursor.getString(1), cursor.getString(2));
    }

    private Klijent uhvatiKlijentaPoUsernamePassword(String username, String password){
        String[] columns = {"id", "username, password"};
        Cursor cursor = db.query("klijent", columns, "username = '" + username + "' AND password = '" + password + "'", null, null, null, null);
        cursor.moveToFirst();
        return new Klijent(new Integer(cursor.getString(0)), cursor.getString(1), cursor.getString(2));
    };

    private boolean klijentPostojiUBazi(int id){
        String[] columns = {"id", "username", "password"};
        Cursor cursor = db.query("klijent", columns, "id = " + id, null, null, null, null);
        return cursor.getCount() > 0;
    }

    public int ubaciPodatke(String telefon, String adresa, String email, int kat){
        if(telefon.length()>20 || adresa.length() > 50 || email.length() > 50 || kat < 0){
        }
        ContentValues cv = new ContentValues();
        cv.put("telefon", telefon);
        cv.put("adresa", adresa);
        cv.put("email", email);
        cv.put("kat", kat);
        return odradiInsertSPovratkom(cv, "podaci");
    }

    public Podaci dohvatePodatkeZaID(int id){
        String [] columns = {"id", "telefon", "adresa", "email", "kat"};
        Cursor cursor = db.query("podaci", columns, "id = " + id, null, null, null, null);
        cursor.moveToFirst();
        return new Podaci(new Integer(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3),
                new Integer(cursor.getString(4)));
    }

    public int ubaciNarudzbu(int podaci){
        ContentValues cv =new ContentValues();
        cv.put("podaciID", podaci);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy. HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        cv.put("vrijeme", formattedDate);
        return odradiInsertSPovratkom(cv, "narudzba");
    }

    public Narudzba dohvatiNarudzbu(int podaciID){
        String [] columns = {"id", "podaciID", "preuzeta", "spremna", "odnesena", "isporucena", "vrijeme", "djelatnik"};
        Cursor cursor = db.query("narudzba", columns, "podaciID = "+podaciID, null, null, null, null);
        cursor.moveToFirst();
        boolean preuzeta = false, spremna =  false, odnesena = false, isporucena = false;
        if(!cursor.isNull(2)){
            preuzeta = true;
        }
        if(!cursor.isNull(3)) {
            spremna = true;
        }
        if(!cursor.isNull(4)){
            odnesena = true;
        }
        if(!cursor.isNull(5)){
            isporucena = true;
        }
        return new Narudzba(new Integer(cursor.getString(0)), new Integer(cursor.getString(1)),
                preuzeta, spremna, odnesena, isporucena, cursor.getString(6), cursor.getString(7));
    }

    public void promijeniCijenuJela(String jelo, double cijena){
        ContentValues cv = new ContentValues();
        cv.put("cijena", cijena);
        db.update("jelo", cv, "ime = '"+jelo+"'", null);
    }

    private void odradiPosaoNarudzbe(int id, String posao, String djelatnik){
        ContentValues cv = new ContentValues();
        cv.put(posao, 1);
        cv.put("djelatnik", djelatnik);
        db.update("narudzba", cv, "id = " + id, null);
    }

    public void preuzmiNarudzbu(int id, String djelatnik){
        odradiPosaoNarudzbe(id, "preuzeta", djelatnik);
    }

    public void preuzmiNarudzbu(Narudzba narudzba, String djelatnik){
        narudzba.isporuci();
        preuzmiNarudzbu(narudzba.getId(), djelatnik);
    }

    public void narudzbaSpremna(int id, String djelatnik){
        odradiPosaoNarudzbe(id, "spremna", djelatnik);
    }

    public void narudzbaSpremna(Narudzba narudzba, String djelatnik){
        narudzba.spremna();
        narudzbaSpremna(narudzba.getId(), djelatnik);
    }

    public void odnesiNarudzbu(int id, String djelatnik){
        odradiPosaoNarudzbe(id, "odnesena", djelatnik);
    }

    public void odnesiNarudzbu(Narudzba narudzba, String djelatnik){
        narudzba.odnesi();
        odnesiNarudzbu(narudzba.getId(), djelatnik);
    }

    public void narudzbaIsporucena(int id, String djelatnik){
        odradiPosaoNarudzbe(id, "isporucena", djelatnik);
    }

    public void narudzbaIsporucena(Narudzba narudzba, String djelatnik){
        narudzba.isporuci();
        narudzbaIsporucena(narudzba.getId(), djelatnik);
    }

    public void ubaciDjelatnika(String username, String password, boolean jeVlasnik) {
        if (username.length() > 20 || password.length() > 20) {
        }
        ContentValues cv = new ContentValues();
        if(jeVlasnik){
            cv.put("vlasnik", 1);
        }
        cv.put("aktivan", 1);
        cv.put("username", username);
        cv.put("password", password);
        odradiInsert(cv, "djelatnik");
    }

    public Djelatnik uhvatiDjelatnikaPoID(int id){
        String[] columns = {"id", "username", "password", "aktivan", "vlasnik"};
        Cursor cursor = db.query("djelatnik", columns, "id = "+id, null, null, null, null);
        cursor.moveToFirst();
        boolean aktivan = false, vlasnik = false;
        if(!cursor.isNull(3)){
            aktivan = true;
        }
        if(!cursor.isNull(4)){
            vlasnik = true;
        }
        return new Djelatnik(new Integer(cursor.getString(0)), cursor.getString(1), cursor.getString(2), aktivan, vlasnik);
    }

    public Djelatnik uhvatiDjelatnikaPoUsernamePassword(String username, String password){
        String[] columns = {"id", "username, password", "aktivan", "vlasnik"};
        try{
            Cursor cursor = db.query("djelatnik", columns, "username = '"+username+"' AND password = '"+password+"'", null, null, null, null);
            cursor.moveToFirst();
            boolean aktivan = false, vlasnik = false;
            if(!cursor.isNull(3)){
                aktivan = true;
            }
            if(!cursor.isNull(4)){
                vlasnik = true;
            }
            return new Djelatnik(new Integer(cursor.getString(0)), cursor.getString(1), cursor.getString(2), aktivan, vlasnik);
        }
        catch(Exception e){
            return null;
        }
    }

    public boolean usernamePostojiUBazi(String username){
        String[] columns = {"username"};
        try{
            Cursor cursor = db.query("djelatnik", columns, "username = '"+username+"'", null, null, null, null);
            if(cursor.getCount() > 0) {
                return true;
            }
        }
        catch (Exception e){}
        return false;
    }

    public Set<String> sviUsernameovi(){
        String[] columns = {"username"};
        Set<String> set = new HashSet<String>();
        Cursor cursor = db.query("djelatnik", columns, null, null, null, null, null);
        cursor.moveToFirst();
        do{
            set.add(cursor.getString(0));
        }
        while(cursor.moveToNext());
        return set;
    }

    public boolean aktivan(String username){
        String[] columns = {"aktivan"};
        Cursor cursor = db.query("djelatnik", columns, "username = '"+username+"'", null, null, null, null);
        cursor.moveToFirst();
        if(cursor.isNull(0)) {
            return false;
        }
        return true;
    }

    public void deaktivirajDjelatnika(String username){
        String uvjet = "username = '"+username+"'";
        ContentValues cv = new ContentValues();
        cv.putNull("aktivan");
        db.update("djelatnik", cv, uvjet, null);
    }

    public void aktivirajDjelatnika(String username){
        String uvjet = "username = '"+username+"'";
        ContentValues cv = new ContentValues();
        cv.put("aktivan", 1);
        db.update("djelatnik", cv, uvjet, null);
    }

    public void ubaciKategoriju(String ime, String username, String opis, int slikaID){
        if (username.length() > 20 || ime.length() > 20) {

        }
        ContentValues cv = new ContentValues();
        cv.put("ime", ime);
        cv.put("dodao", username);
        cv.put("opis", opis);
        cv.put("slikaID", slikaID);
        odradiInsert(cv, "kategorija");
    }

    public Kategorija vratiKategoriju(int id){
        String[] columns = {"id", "ime", "dodao", "opis", "slikaID"};
        Cursor cursor = db.query("kategorija", columns, "id = "+id, null, null, null, null);
        cursor.moveToFirst();
        return new Kategorija(new Integer(cursor.getString(0)), cursor.getString(1), cursor.getString(2),
                cursor.getString(3), new Integer(cursor.getString(4)));
    }

    public Kategorija[] vratiSveKategorije(){
        String[] columns = {"id", "ime", "dodao", "opis", "slikaID"};
        Cursor cursor = db.query("kategorija", columns, null, null, null, null, null);
        Kategorija[] kategorije = new Kategorija[cursor.getCount()];
        int i = 0;
        cursor.moveToFirst();
        do{
            kategorije[i] = new Kategorija(new Integer(cursor.getString(0)), cursor.getString(1), cursor.getString(2),
                    cursor.getString(3), new Integer(cursor.getString(4)));
            i++;
        }
        while(cursor.moveToNext());
        return kategorije;
    }

    public int vratiIDKategorijeZaIme(String imeKategorije){
        String[] columns = {"id"};
        Cursor cursor = db.query("kategorija", columns, "ime = '"+imeKategorije+"'", null, null, null, null);
        cursor.moveToFirst();
        return Integer.parseInt(cursor.getString(0));
    }

    public String vratiImeKategorijeZaID(int id){
        String[] columns = {"ime"};
        Cursor cursor = db.query("kategorija", columns, "id = "+id, null, null, null, null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }


    public void ubaciJelo(String ime, String opis, double cijena, String kategorija, boolean dostupno, int slikaID,
                          String username, int brojNarucivanja){
        if (username.length() > 20 || ime.length() > 20) {
        }
        int kategorijaID = vratiIDKategorijeZaIme(kategorija);
        ContentValues cv = new ContentValues();
        cv.put("ime", ime);
        cv.put("kategorija", kategorijaID);
        cv.put("brojNarucivanja", brojNarucivanja);
        if(dostupno){
            cv.put("dostupno", 1);
        }
        cv.put("zadnjaIzmjena", username);
        cv.put("cijena", cijena);
        cv.put("opis", opis);
        cv.put("slikaID", slikaID);
        odradiInsert(cv, "jelo");
    }

    public void ubaciJelo(String ime, String opis, double cijena, String kategorija, boolean dostupno, int slikaID,
                          String username, int brojNarucivanja, String [] kars){
        if (username.length() > 20 || ime.length() > 20) {
        }
        int kategorijaID = vratiIDKategorijeZaIme(kategorija);
        ContentValues cv = new ContentValues();
        cv.put("ime", ime);
        cv.put("kategorija", kategorijaID);
        cv.put("brojNarucivanja", brojNarucivanja);
        if(dostupno){
            cv.put("dostupno", 1);
        }
        cv.put("zadnjaIzmjena", username);
        cv.put("cijena", cijena);
        cv.put("opis", opis);
        cv.put("slikaID", slikaID);
        int jeloID = odradiInsertSPovratkom(cv, "jelo");
        dodajKarakteristikeJela(jeloID, kars);
    }

    public Jelo vratiJelo(int id){
        String[] columns = {"id", "ime", "opis", "cijena", "kategorija", "dostupno", "zadnjaIzmjena", "slikaID", "brojNarucivanja"};
        Cursor cursor = db.query("jelo", columns, "id = " + id, null, null, null, null);
        cursor.moveToFirst();
        boolean dostupno = false;
        if(!cursor.isNull(5)){
            dostupno = true;
        }
        int idZaBazu = new Integer(cursor.getString(0));
        String ime = cursor.getString(1);
        String opis = cursor.getString(2);
        float cijena = new Float(cursor.getString(3));
        int kategorijaID = new Integer(cursor.getString(4));
        String zadnjaIzmjena = new String(cursor.getString(6));
        int slikaID = new Integer(cursor.getString(7));
        int brojNarucivanja = new Integer(cursor.getString(8));

        return new Jelo(idZaBazu, ime, opis, cijena, kategorijaID, dostupno, zadnjaIzmjena, slikaID, vratiKarakteristikeJela(idZaBazu), brojNarucivanja);
    }

    public Jelo[] vratiSvaJela(){
        String[] columns = {"id", "ime", "opis", "cijena", "kategorija", "dostupno", "zadnjaIzmjena", "slikaID", "brojNarucivanja"};
        Cursor cursor = db.query("jelo", columns, null, null, null, null, null);
        Jelo[] jela = new Jelo[cursor.getCount()];
        int i = 0;
        cursor.moveToFirst();
        do{
            boolean dostupno = false;
            if(!cursor.isNull(5)){
                dostupno = true;
            }
            int idZaBazu = new Integer(cursor.getString(0));
            String ime = cursor.getString(1);
            String opis = cursor.getString(2);
            float cijena = new Float(cursor.getString(3));
            int kategorijaID = new Integer(cursor.getString(4));
            String zadnjaIzmjena = new String(cursor.getString(6));
            int slikaID = new Integer(cursor.getString(7));
            int brojNarucivanja = new Integer(cursor.getString(8));
            jela[i] = new Jelo(idZaBazu, ime, opis, cijena, kategorijaID, dostupno, zadnjaIzmjena, slikaID, brojNarucivanja);
            jela[i].dodajSveKarakteristike(vratiKarakteristikeJela(idZaBazu));
            i++;
        }
        while(cursor.moveToNext());
        return jela;
    }

    public void postaviJeloNedostupno(String jelo){
        String uvjet = "ime = '"+jelo+"'";
        ContentValues cv = new ContentValues();
        cv.putNull("dostupno");
        db.update("jelo", cv, uvjet, null);
    }

    public void postaviJeloDostupno(String jelo){
        String uvjet = "ime = '"+jelo+"'";
        ContentValues cv = new ContentValues();
        cv.put("dostupno", 1);
        db.update("jelo", cv, uvjet, null);
    }

    public Jelo[] vratiDostupnaJela(){
        String[] columns = {"id", "ime", "opis", "cijena", "kategorija", "dostupno", "zadnjaIzmjena", "slikaID", "brojNarucivanja"};
        Cursor cursor = db.query("jelo", columns, "dostupno NOT NULL", null, null, null, null);
        Jelo[] jela = new Jelo[cursor.getCount()];
        int i = 0;
        cursor.moveToFirst();
        do{
            boolean dostupno = false;
            if(!cursor.isNull(5)){
                dostupno = true;
            }
            int idZaBazu = new Integer(cursor.getString(0));
            String ime = cursor.getString(1);
            String opis = cursor.getString(2);
            float cijena = new Float(cursor.getString(3));
            int kategorijaID = new Integer(cursor.getString(4));
            String zadnjaIzmjena = new String(cursor.getString(6));
            int slikaID = new Integer(cursor.getString(7));
            int brojNarucivanja = new Integer(cursor.getString(8));
            jela[i] = new Jelo(idZaBazu, ime, opis, cijena, kategorijaID, dostupno, zadnjaIzmjena, slikaID, brojNarucivanja);
            jela[i].dodajSveKarakteristike(vratiKarakteristikeJela(idZaBazu));
            i++;
        }
        while(cursor.moveToNext());
        return jela;
    }

    public List<String> vratiKarakteristikeJela(int idZaBazu){
        List<String> karakteristike = new LinkedList<String>();
        String[] columns = {"kar", "jelo"};
        Cursor cursor = db.query("jeloKarakteristika", columns, "jelo = "+idZaBazu, null, null, null, null);
        cursor.moveToFirst();
        try{
            do{
                String[] col = {"opis"};
                int kar = new Integer(cursor.getString(0));
                Cursor temp = db.query("karakteristika", col, "id = "+kar, null, null, null, null);
                temp.moveToFirst();
                karakteristike.add(temp.getString(0));
            }
            while(cursor.moveToNext());
        }catch(Exception e){

        }

        return  karakteristike;
    }

    public void dodajKarakteristikeJela(int jelo, String[] kars){
        if(jelo==-1){
            return;
        }
        for(String kar : kars){
            ContentValues cv = new ContentValues();
            int rbrKar = sadrziKarakteristiku(kar);
            if(rbrKar == -1){
                cv.put("opis", kar);
                rbrKar = odradiInsertSPovratkom(cv, "karakteristika");
                cv = new ContentValues();
            }
            cv.put("kar", rbrKar);
            cv.put("jelo", jelo);
            odradiInsert(cv, "jeloKarakteristika");
        }
    }

    public void dodajKarakteristikuJela(int jelo, String kar){
        if(jelo==-1){
            return;
        }
        ContentValues cv = new ContentValues();
        int rbrKar = sadrziKarakteristiku(kar);
        if(rbrKar == -1){
            cv.put("opis", kar);
            rbrKar = odradiInsertSPovratkom(cv, "karakteristika");
            cv = new ContentValues();
        }
        cv.put("kar", rbrKar);
        cv.put("jelo", jelo);
        odradiInsert(cv, "jeloKarakteristika");
    }

    public List<String> vratiSveKarakteristike(){
        List<String> kar = new LinkedList<String>();
        String[] columns = {"id", "opis"};
        Cursor cursor = db.query("karakteristika", columns, null, null, null, null, null);
        cursor.moveToFirst();
        do{
            kar.add(cursor.getString(1));
        }
        while(cursor.moveToNext());
        return  kar;
    }

    private int sadrziKarakteristiku(String kar){
        String[] columns = {"id", "opis"};
        try{
            Cursor cursor = db.query("karakteristika", columns, "opis = '"+kar+"'", null, null, null, null);
            cursor.moveToFirst();
            return new Integer(cursor.getString(0));
        }
        catch (Exception e){
            return -1;
        }
    }

    public void narudzbaJelo(int narudzba, int jelo, int kolicina, List<String> kars){
        if(kolicina < 0){
        }
        ContentValues cv = new ContentValues();
        cv.put("narudzba", narudzba);
        cv.put("jelo", jelo);
        cv.put("kolicina", kolicina);
        int narudzbaJelo = odradiInsertSPovratkom(cv, "narudzbaJelo");

        for(String kar : kars){
            cv = new ContentValues();
            cv.put("narudzbaJelo", narudzbaJelo);
            cv.put("jeloKarakteristika", kar);
            odradiInsert(cv, "narudzbaJeloKarakteristika");
        }

        String[] columns = {"brojNarucivanja"};
        Cursor cursor = db.query("jelo", columns, "id = "+jelo, null, null, null, null);
        cursor.moveToFirst();
        int brojNarucivanja = new Integer(cursor.getString(0));
        brojNarucivanja += kolicina;
        cv = new ContentValues();
        cv.put("brojNarucivanja", brojNarucivanja);
        db.update("jelo", cv, "id = "+jelo, null);
    }

    public NarudzbaJelo[] jelaNarudzbe(int narudzba){
        String[] columns = {"id", "narudzba", "jelo", "kolicina"};
        Cursor cursor = db.query("narudzbaJelo", columns, "narudzba = " + narudzba, null, null, null, null);
        if(cursor.getCount() == 0){
            return null;
        }
        NarudzbaJelo[] jelaNarudzbe = new NarudzbaJelo[cursor.getCount()];
        int i = 0;
        cursor.moveToFirst();
        do{
            int narudzbaJelo = new Integer(cursor.getString(0));
            String[] cols = {"narudzbaJelo", "jeloKarakteristika"};
            Cursor crs = db.query("narudzbaJeloKarakteristika", cols, "narudzbaJelo = "+narudzbaJelo, null, null, null, null);
            crs.moveToFirst();
            List<String> list = new LinkedList<String>();
            do{
                try{
                    String kar = crs.getString(1);
                    list.add(kar);
                }
                catch (Exception e){
                }
            }
            while(crs.moveToNext());
            jelaNarudzbe[i] = new NarudzbaJelo(narudzbaJelo, new Integer(cursor.getString(1)),
                    new Integer(cursor.getString(2)), new Integer(cursor.getString(3)), list);
            i++;
        }
        while(cursor.moveToNext());
        return jelaNarudzbe;
    }

    public List<NarudzbaJelo> sveNarudzbaJelo(){
        String[] columns = {"id", "narudzba", "jelo", "kolicina"};
        Cursor cursor = db.query("narudzbaJelo", columns, null, null, null, null, null);
        List<NarudzbaJelo> retList = new LinkedList<NarudzbaJelo>();
        cursor.moveToFirst();
        do{
            int narudzbaJelo = new Integer(cursor.getString(0));
            String[] cols = {"narudzbaJelo", "jeloKarakteristika"};
            Cursor crs = db.query("narudzbaJeloKarakteristika", cols, "narudzbaJelo = "+narudzbaJelo, null, null, null, null);
            crs.moveToFirst();
            List<String> list = new LinkedList<String>();
            do{
                try{
                    String kar = crs.getString(1);
                    list.add(kar);
                }
                catch (Exception e){
                }
            }
            while(crs.moveToNext());
            retList.add(new NarudzbaJelo(narudzbaJelo, new Integer(cursor.getString(1)),
                    new Integer(cursor.getString(2)), new Integer(cursor.getString(3)), list));
        }
        while(cursor.moveToNext());
        return retList;
    }

    public void ubaciRecenziju(Integer ocjena, String sadrzaj, String username, String datum){
        if(sadrzaj.length() > 400){
        }
        ContentValues cv = new ContentValues();
        if(ocjena != null){
            if(ocjena < 1 || ocjena > 5){
            }
            cv.put("ocjena", ocjena);
        }
        if(sadrzaj == null){
            sadrzaj = new String("");
        }
        cv.put("sadrzaj", sadrzaj);
        cv.put("username", username);
        cv.put("datum", datum);
        odradiInsert(cv, "recenzija");
    }

    public Recenzija[] izvuciSveRecenzije(){
        String[] columns = {"id", "ocjena", "sadrzaj", "username", "datum"};
        Cursor cursor = db.query("recenzija", columns, null, null, null, null, null);
        Recenzija[] recenzije = new Recenzija[cursor.getCount()];
        int i = 0;
        cursor.moveToFirst();
        try{
            do{
                Integer ocjena = null;
                if(!cursor.isNull(1)){
                    ocjena = new Integer(cursor.getString(1));
                }
                String sadrzaj = cursor.getString(2);
                recenzije[i] = new Recenzija(new Integer(cursor.getString(0)), ocjena, sadrzaj, cursor.getString(3), cursor.getString(4));
                i++;
            }
            while(cursor.moveToNext());
        }catch (Exception e){

        }

        return recenzije;
    }


    public void spremiInformaciju(String kljucnaRijec, String informacija){
        ContentValues cv = new ContentValues();
        cv.put("kljucna", kljucnaRijec);
        cv.put("informacija", informacija);
        odradiInsert(cv, "informacija");
    }

    public String procitajInformaciju(String klucnaRijec){ //vraca null ako nije pronadjeno
        try{
            String[] columns = {"kljucna", "informacija"};
            Cursor cursor = db.query("informacija", columns, "kljucna = '"+klucnaRijec+"'", null, null, null, null);
            cursor.moveToFirst();
            return cursor.getString(1);
        }
        catch (Exception e){
            return null;
        }
    }

    public boolean izbrisiInformaciju(String kljucnaRijec){
        return db.delete("informacija", "kljucna = '"+kljucnaRijec+"'", null) > 0;
    }

    public List<Narudzba> vratiSveNarudzbe(){
        String [] columns = {"id", "podaciID", "preuzeta", "spremna", "odnesena", "isporucena", "vrijeme", "djelatnik"};
        Cursor cursor = db.query("narudzba", columns, null, null, null, null, null);
        cursor.moveToFirst();
        List<Narudzba> narudzbe = new LinkedList<Narudzba>();
        if(cursor.getCount() == 0){
            return narudzbe;
        }
        do{
            boolean preuzeta = false, spremna =  false, odnesena = false, isporucena = false;
            if(!cursor.isNull(2)){
                preuzeta = true;
            }
            if(!cursor.isNull(3)) {
                spremna = true;
            }
            if(!cursor.isNull(4)){
                odnesena = true;
            }
            if(!cursor.isNull(5)){
                isporucena = true;
            }
            narudzbe.add(new Narudzba(new Integer(cursor.getString(0)), new Integer(cursor.getString(1)),
                    preuzeta, spremna, odnesena, isporucena, cursor.getString(6), cursor.getString(7)));
        }
        while(cursor.moveToNext());
        return narudzbe;
    }


    public void update(){
        //vise nisam siguran otkud sam ovo kopirao, mislim da je beskorisna
    }
}
