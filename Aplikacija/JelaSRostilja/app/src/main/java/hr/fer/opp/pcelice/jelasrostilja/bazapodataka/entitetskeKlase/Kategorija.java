package hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase;

/**
 * Created by Murta on 30.12.2015..
 */
public class Kategorija {

    private Integer id;
    private String ime;
    private String dodao;
    private String opis;
    private int slikaID;

    public Kategorija(String ime, String dodao, String opis, int slikaID){
        this(null, ime, dodao, opis, slikaID);
    }

    public Kategorija(Integer id, String ime, String dodao, String opis, int slikaID){
        if(ime.length() > 20){
            //TODO once again
        }
        if(id == null){
            this.id = null;
        }
        else{
            this.id = new Integer(id);
        }
        this.ime = ime;
        this.dodao = dodao;
        this.opis = opis;
        this.slikaID = slikaID;
    }

    public Integer getId() {
        return id;
    }

    public String getIme() {
        return ime;
    }

    public String getDodao() {
        return dodao;
    }

    public String getOpis() {return opis;}

    public int getSlikaID() {
        return slikaID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Kategorija that = (Kategorija) o;

        return !(id != null ? !id.equals(that.id) : that.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
