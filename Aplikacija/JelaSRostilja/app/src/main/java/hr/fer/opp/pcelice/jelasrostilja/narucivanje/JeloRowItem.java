package hr.fer.opp.pcelice.jelasrostilja.narucivanje;

import java.text.DecimalFormat;

import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Jelo;

public class JeloRowItem {
    private Jelo jelo;
    private DecimalFormat formatter = new DecimalFormat("#.00 kn");

    public JeloRowItem(Jelo jelo) {
        this.jelo = jelo;
    }
    public int getSlikaID() {
        return jelo.getSlikaID();
    }

    public String getOpisJela() {
        return jelo.getOpisJela();
    }
    public String getNaslov() {
        return (jelo.getIme() + " - " + formatter.format(jelo.getCijena()));
    }
    public Jelo getJelo() { return jelo; }

    @Override
    public String toString() {
        return this.getNaslov();
    }
}