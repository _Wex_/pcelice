package hr.fer.opp.pcelice.jelasrostilja.naslovna;


import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hr.fer.opp.pcelice.jelasrostilja.R;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.PrivremenaBaza;

public class PrikaznikDjelatnikJeloRowItema extends ArrayAdapter<DjelatnikJeloRowItem> {

    Context context;

    public PrikaznikDjelatnikJeloRowItema(Context context, int resourceId,
                                 List<DjelatnikJeloRowItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView slikaJela;
        TextView naslovJela;
        TextView opisJela;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        DjelatnikJeloRowItem jeloRowItem = getItem(position);
        int brojNarucivanja = jeloRowItem.getJelo().getBrojNarucivanja();

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.djelatnik_jela_list_item, null);
            holder = new ViewHolder();

            holder.opisJela = (TextView) convertView.findViewById(R.id.upravljanjeJelimaItemDostupno);
            holder.naslovJela = (TextView) convertView.findViewById(R.id.upravljanjeJelimaItemNaslov);
            holder.slikaJela = (ImageView) convertView.findViewById(R.id.upravljanjeJelimaItemSlika);

            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        String opis = "";
        if(jeloRowItem.getJelo().jeDostupno()){
            opis += "Jelo je dostupno.\n";
        }else{
            opis += "Jelo nije dostupno.\n";
        }
        if(brojNarucivanja > 2 * PrivremenaBaza.prosjekNarucivanjaJela){
            opis += "Cesto naručivano.";
        }else{
            opis += "Nije često naručivano.";
        }
        holder.opisJela.setText(opis);
        holder.naslovJela.setText(jeloRowItem.getNaslov());
        try {
            holder.slikaJela.setImageResource(jeloRowItem.getSlikaID());
        } catch (Exception e) {
            try {
                String adresa = PrivremenaBaza.baza.procitajInformaciju("slika" + jeloRowItem.getSlikaID());
                holder.slikaJela.setImageURI(Uri.parse(adresa));

            } catch (Exception ex) {
            }
        }
        return convertView;
    }
}




