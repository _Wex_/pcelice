package hr.fer.opp.pcelice.jelasrostilja.komentari;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.Time;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hr.fer.opp.pcelice.jelasrostilja.R;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.PrivremenaBaza;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Recenzija;


public class KomentariActivity extends Activity implements
        OnItemClickListener {

    private Button buttonDodajKomentar = null;
    ListView listView;
    List<KomentarRowItem> komentarRowItems;

    PrikaznikKomentarRowItema adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_komentari);

        komentarRowItems = new ArrayList<KomentarRowItem>();

        Recenzija[] recenzije = new Recenzija[0];
        try{
            recenzije = PrivremenaBaza.baza.izvuciSveRecenzije();
        } catch(Exception e){
            System.out.print("boo");
        }

        for(Recenzija recenzija : recenzije){
            komentarRowItems.add(new KomentarRowItem(recenzija));
        }

        listView = (ListView) findViewById(R.id.komentarListaKomentara);
        adapter = new PrikaznikKomentarRowItema(this,
                R.layout.komentar_list_item, komentarRowItems);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        buttonDodajKomentar = (Button)findViewById(R.id.komentarDodajKomentar);
        Intent arrivingIntent = getIntent();
        if(arrivingIntent.getBooleanExtra("Djelatnik", false)){
            buttonDodajKomentar.setVisibility(View.INVISIBLE);
            buttonDodajKomentar.setEnabled(false);
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        Toast toast = Toast.makeText(getApplicationContext(),
                komentarRowItems.get(position).getRecenzija().getSadrzaj(),
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    public void dodajKomentar(View view){

        setContentView(R.layout.novi_komentar);

    }

    public void posaljiKomentar(View view){

        EditText tekstKomentara = (EditText) findViewById(R.id.komentarTekstKomentara);
        RatingBar rating = (RatingBar) findViewById(R.id.komentarRatingBar);
        EditText korisnickoImeEdit = (EditText) findViewById(R.id.komentarEditKorisnik);
        String korisnickoIme;
        if(korisnickoImeEdit.getText().toString().equals("")){
            korisnickoIme = "Anonimni korisnik";
        }else{
            korisnickoIme = korisnickoImeEdit.getText().toString();
        }
        Integer ocjena;
        if(rating.getRating() == 0){
            ocjena = null;
        }else{
            ocjena = Math.round(rating.getRating());
        }

        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();

        if(tekstKomentara.getText().length() == 0){
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Komentar nemoze biti prazan!",
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }else{
            PrivremenaBaza.baza.ubaciRecenziju(ocjena, tekstKomentara.getText().toString(), korisnickoIme,  today.format("%d.%m.%Y. %H:%M"));
            setContentView(R.layout.activity_komentari);

            // TODO LUKA: ovo je uzasno sporo treba prepraviti da se mjenja layout ali to ne osvjezava komentare iz nekog razloga
            Intent nova = new Intent(this, KomentariActivity.class);
            startActivity(nova);
            finish();
        }


    }



}
