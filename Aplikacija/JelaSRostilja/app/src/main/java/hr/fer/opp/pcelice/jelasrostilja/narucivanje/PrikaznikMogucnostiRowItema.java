package hr.fer.opp.pcelice.jelasrostilja.narucivanje;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import hr.fer.opp.pcelice.jelasrostilja.R;

/**
 * Created by luka on 17.01.16..
 */

public class PrikaznikMogucnostiRowItema extends ArrayAdapter<MogucnostiRowItem> {

    Context context;

    public PrikaznikMogucnostiRowItema(Context context, int resourceId,
                                 List<MogucnostiRowItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        TextView mogucnost;
        CheckBox checkBox;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        MogucnostiRowItem mogucnost = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.mogucnosti_list_item, null);
            holder = new ViewHolder();
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.mogucnostiItemCheckbox);
            holder.mogucnost = (TextView) convertView.findViewById(R.id.mogucnostiItemOpis);

            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.mogucnost.setText(mogucnost.mogucnost);

        return convertView;
    }
}
