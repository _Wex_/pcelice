package hr.fer.opp.pcelice.jelasrostilja.bazapodataka;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Murta on 29.12.2015..
 */
public class DatabaseOperations extends SQLiteOpenHelper{

    static final int database_version = 32;
    private final String klijent = "CREATE TABLE IF NOT EXISTS klijent (id INTEGER PRIMARY KEY, " +
            "username VARCHAR(20) UNIQUE, password VARCHAR(20) );";
    private final String podaci = "CREATE TABLE IF NOT EXISTS podaci (id INTEGER PRIMARY KEY, " +
            "telefon VARCHAR(20) NOT NULL, " +
            "adresa VARCHAR(50) NOT NULL, email VARCHAR(50) NOT NULL, kat INTEGER NOT NULL);";
    private final String narudzba = "CREATE TABLE IF NOT EXISTS narudzba (id INTEGER PRIMARY KEY, " +
            "podaciID INTEGER NOT NULL REFERENCES podaci(id), preuzeta INTEGER DEFAULT NULL, " +
            "spremna INTEGER DEFAULT NULL, odnesena INTEGER DEFAULT NULL, isporucena INTEGER DEFAULT NULL, " +
            "vrijeme VARCHAR(30) NOT NULL, djelatnik VARCHAR (20) REFERENCES djelatnik(username));";
    private final String djelatnik = "CREATE TABLE IF NOT EXISTS djelatnik (id INTEGER PRIMARY KEY, " +
            "username VARCHAR(20), password VARCHAR(20), aktivan INTEGER DEFAULT 1, vlasnik INTEGER DEFAULT NULL, " +
            "CONSTRAINT djeluniq UNIQUE(username) ON CONFLICT IGNORE);";
    private final String kategorija = "CREATE TABLE IF NOT EXISTS kategorija (id INTEGER PRIMARY KEY, " +
            "dodao VARCHAR(20) NOT NULL REFERENCES djelatnik(username), ime VARCHAR(20) NOT NULL, opis VARCHAR(200) NOT NULL, " +
            "slikaID INTEGER NOT NULL, CONSTRAINT katuniq UNIQUE(ime) ON CONFLICT IGNORE);";
    private final String jelo = "CREATE TABLE IF NOT EXISTS jelo (id INTEGER PRIMARY KEY, " +
            "kategorija INTEGER NOT NULL REFERENCES kategorija(id), dostupno INTEGER DEFAULT NULL, " +
            "zadnjaIzmjena VARCHAR(20) REFERENCES djelatnik(username), ime VARCHAR(20) NOT NULL, " +
            "cijena REAL NOT NULL, opis VARCHAR(200) NOT NULL, slikaID INTEGER NOT NULL, " +
            " brojNarucivanja INTEGER DEFAULT 0 NOT NULL, CONSTRAINT jelouniq UNIQUE(ime) ON CONFLICT IGNORE);";
    private final String narudzbaJelo = "CREATE TABLE IF NOT EXISTS narudzbaJelo (id INTEGER PRIMARY KEY, " +
            "narudzba INTEGER NOT NULL REFERENCES narudzba(id), jelo INTEGER NOT NULL REFERENCES jelo(id), kolicina INTEGER NOT NULL);";
    private final String recenzija = "CREATE TABLE IF NOT EXISTS recenzija (id INTEGER PRIMARY KEY, " +
            "ocjena INTEGER DEFAULT NULL CHECK (ocjena > 0 AND ocjena < 6), " +
            "sadrzaj VARCHAR(400) NOT NULL, username VARCHAR(30) NOT NULL, datum VARCHAR(30) NOT NULL);";
    private final String karakteristika = "CREATE TABLE IF NOT EXISTS karakteristika (id INTEGER PRIMARY KEY, " +
            "opis VARCHAR(30) NOT NULL, CONSTRAINT karuniq UNIQUE(opis) ON CONFLICT IGNORE);";
    private String jeloKarakteristika = "CREATE TABLE IF NOT EXISTS jeloKarakteristika (id INTEGER PRIMARY KEY, " +
            "kar INTEGER NOT NULL REFERENCES karakteristika(id), jelo INTEGER NOT NULL REFERENCES jelo(id));";
    private String narudzbaJeloKarakteristika = "CREATE TABLE IF NOT EXISTS narudzbaJeloKarakteristika (id INTEGER PRIMARY KEY, " +
            "narudzbaJelo INTEGER NOT NULL REFERENCES narudzbaJelo(id), jeloKarakteristika INTEGER NOT NULL REFERENCES jeloKarakteristika(id));";
    private final String informacija = "CREATE TABLE IF NOT EXISTS informacija (id INTEGER PRIMARY KEY, " +
            "kljucna VARCHAR(30) NOT NULL, informacija VARCHAR(200) NOT NULL, CONSTRAINT infuniq UNIQUE(kljucna) ON CONFLICT IGNORE);";

    public DatabaseOperations(Context context) {
        super(context, TableInfo.DATABASE, null, database_version);
        Log.d("Database operations", "Database created");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(klijent);
        db.execSQL(podaci);
        db.execSQL(narudzba);
        db.execSQL(djelatnik);
        db.execSQL(kategorija);
        db.execSQL(jelo);
        db.execSQL(narudzbaJelo);
        db.execSQL(recenzija);
        db.execSQL(karakteristika);
        db.execSQL(jeloKarakteristika);
        db.execSQL(narudzbaJeloKarakteristika);
        db.execSQL(informacija);
        Log.d("Database operations", "Tables created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE klijent");
        db.execSQL("DROP TABLE podaci");
        db.execSQL("DROP TABLE narudzba");
        db.execSQL("DROP TABLE djelatnik");
        db.execSQL("DROP TABLE kategorija");
        db.execSQL("DROP TABLE jelo");
        db.execSQL("DROP TABLE narudzbajelo");
        db.execSQL("DROP TABLE recenzija");
        db.execSQL("DROP TABLE karakteristika");
        db.execSQL("DROP TABLE jeloKarakteristika");
        db.execSQL("DROP TABLE narudzbaJeloKarakteristika");
        db.execSQL("DROP TABLE informacija");
        onCreate(db);
    }

}
