package hr.fer.opp.pcelice.jelasrostilja.komentari;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import hr.fer.opp.pcelice.jelasrostilja.R;

public class PrikaznikKomentarRowItema extends ArrayAdapter<KomentarRowItem> {

    Context context;

    public PrikaznikKomentarRowItema(Context context, int resourceId,
                                     List<KomentarRowItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        TextView ocjena;
        TextView sadrzaj;
        TextView vrijeme;
        TextView korisnickoIme;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        KomentarRowItem komentarRowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.komentar_list_item, null);
            holder = new ViewHolder();
            holder.ocjena = (TextView) convertView.findViewById(R.id.komentarItemOcjena);
            holder.sadrzaj = (TextView) convertView.findViewById(R.id.komentarItemSadrzaj);
            holder.vrijeme = (TextView) convertView.findViewById(R.id.komentarItemVrijeme);
            holder.korisnickoIme = (TextView) convertView.findViewById(R.id.komentarItemKorisnickoIme);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        if(komentarRowItem.getRecenzija().getOcjena() == null){
            holder.ocjena.setText("<neocjenjeno>");
        }else{
            holder.ocjena.setText("Ocjena: " + komentarRowItem.getRecenzija().getOcjena());
        }
        holder.korisnickoIme.setText(komentarRowItem.getRecenzija().getUsername());
        holder.vrijeme.setText(komentarRowItem.getRecenzija().getDatum());
        holder.sadrzaj.setText(komentarRowItem.getRecenzija().getSadrzaj());

        return convertView;
    }
}