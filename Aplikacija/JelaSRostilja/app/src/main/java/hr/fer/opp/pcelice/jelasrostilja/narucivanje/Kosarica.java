package hr.fer.opp.pcelice.jelasrostilja.narucivanje;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Jelo;

/**
 * Created by luka on 04.01.16..
 */

public class Kosarica {
    // sadrzaj kosarice, svakom jelu je pridruzena kolicina jela
    private static Map<Jelo, Integer> sadrzaj = new HashMap<>();

    // vraca ukupnu vrijednost kosarice, umanjenu za dani popust
    public static double vrijednostKosarice(double popust){
        Set<Jelo> sadrzanaJela = sadrzaj.keySet();
        double ukupno = 0.0;
        for(Jelo jelo : sadrzanaJela){
            ukupno += jelo.getCijena() * sadrzaj.get(jelo);
        }
        return ukupno * (1 - popust);
    }
    // za odredjeno jelo, vraca kolicinu tog jela u kosarici
    public static Integer kolicinaJela(Jelo jelo){
        return sadrzaj.get(jelo);
    }
    // vraca koja su jela sadrzana u kosarici
    public static List<Jelo> sadrzanaJela(){
        return new ArrayList<Jelo>(sadrzaj.keySet());
    }
    // dodaje odredjeno jelo s danom kolicinom u kosaricu
    public static void dodajUKosaricu(Jelo jelo, int kolicina){
        if(sadrzaj.containsKey(jelo)){
            int trenutnaKolicina = sadrzaj.get(jelo);
            sadrzaj.put(jelo, trenutnaKolicina + kolicina);
        }
        else{
            sadrzaj.put(jelo, kolicina);
        }
    }
    // mice odredjeno jelo iz kosarice sa danom kolicinom
    public static void makniIzKosarice(Jelo jelo, int kolicina){
        if(sadrzaj.containsKey(jelo)){
            int trenutnaKolicina = sadrzaj.get(jelo);
            if((trenutnaKolicina - kolicina) > 0){
                sadrzaj.put(jelo, trenutnaKolicina - kolicina);
            }
            else{
                sadrzaj.remove(jelo);
            }
        }
    }
    // brise sadrzaj kosarice
    public static void isprazniKosaricu(){
        sadrzaj = new HashMap<Jelo, Integer>();
    }
    // prilikom potvrde narudzbe vraca sadrzaj kosarice i prazni ju
    public static Map<Jelo, Integer> checkout(){
        Map<Jelo, Integer> povratnaVrijednost = new HashMap<Jelo, Integer>(sadrzaj);
        isprazniKosaricu();
        return povratnaVrijednost;
    }
}
