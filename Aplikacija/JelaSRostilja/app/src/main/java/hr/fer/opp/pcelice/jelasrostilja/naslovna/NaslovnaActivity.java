package hr.fer.opp.pcelice.jelasrostilja.naslovna;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Random;
import java.util.logging.FileHandler;

import hr.fer.opp.pcelice.jelasrostilja.R;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.PrivremenaBaza;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Djelatnik;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Narudzba;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.NarudzbaJelo;
import hr.fer.opp.pcelice.jelasrostilja.bazapodataka.entitetskeKlase.Recenzija;
import hr.fer.opp.pcelice.jelasrostilja.komentari.KomentariActivity;
import hr.fer.opp.pcelice.jelasrostilja.narucivanje.Kosarica;
import hr.fer.opp.pcelice.jelasrostilja.narucivanje.NarucivanjeActivity;

public class NaslovnaActivity extends AppCompatActivity {

    private Djelatnik logiraniDjelatnik = null;
    private Boolean jeliLogirano = false;
    private double prosjecnaOcjenaRestorana = 0;
    private int mod = 1;
    private int previousMod = 1;
    private boolean loginPopupEnabled = false;
    private boolean errorPopupEnabled = false;
    private View currentContentView = null;

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PrivremenaBaza.inicijalizirajBazu(getApplicationContext());
        Kosarica.isprazniKosaricu();
        initKlijentMod();
    }

    @Override
    public void onBackPressed(){
        if(mod == 1 || mod == 2){
            finish();
        }
        else if(mod == 3){
            initDjelatnikMod();
        }
        else if(mod == 4) {
            initDjelatnikMod();
        }
        else if(mod == 5) {
            initNarudzbe();
        }
    }

    private void initKlijentMod(){
        previousMod = mod;
        mod = 1;
        logiraniDjelatnik = null;
        jeliLogirano = false;
        setContentView(R.layout.activity_naslovna_klijent);
        prosjecnaOcjenaRestorana = izracunajOcjenu();
        RatingBar naslovnaRating = (RatingBar) findViewById(R.id.naslovnaProsjecnaOcjena);
        naslovnaRating.setRating((float) prosjecnaOcjenaRestorana);
        postaviInfoKlijentDjelatnik();
    }

    private void initDjelatnikMod(){
        previousMod = mod;
        mod = 2;
        setContentView(R.layout.activity_naslovna);
        if(logiraniDjelatnik.jeVlasnik()) {
            initInitUredjivanja();
            initInitStatistike();
        }
        prosjecnaOcjenaRestorana = izracunajOcjenu();
        RatingBar naslovnaRating = (RatingBar) findViewById(R.id.naslovnaProsjecnaOcjena);
        naslovnaRating.setRating((float) prosjecnaOcjenaRestorana);
        buttonPreuzimanjeNarudzbi = (Button) findViewById(R.id.buttonNarudzbeAN);
        postaviInfoKlijentDjelatnik();
    }

    private void initNarudzbe(){
        previousMod = mod;
        mod = 3;
        setContentView(R.layout.preuzimanje_narudzbi_screen);
        initNarudzbeDrugiDio();
    }

    private void initUredjivanje(){
        previousMod = mod;
        mod = 4;
        setContentView(R.layout.activity_naslovna_uredi);
        postaviInfoUredjivanje();
    }

    private void initDetaljiNarudzbeMod(){
        previousMod = mod;
        mod = 5;
        setContentView(R.layout.detalji_narudzbe);
        postaviDetaljiNarudzbe();
    }

    private void postaviInfoKlijentDjelatnik(){
        TextView imeRestorana = (TextView)findViewById(R.id.naslovnaImeRestoranaTextView);
        TextView adresaRestorana = (TextView)findViewById(R.id.naslovnaAdresaRestoranaTextView);
        TextView opisRestorana = (TextView)findViewById(R.id.naslovnaOpisRestoranaTextView);
        TextView imeVlasnika = (TextView)findViewById(R.id.naslovnaImeVlasnikaTextView);
        TextView radnoVrijeme = (TextView)findViewById(R.id.naslovnaRadnoVrijemeTextView);
        TextView telefon = (TextView)findViewById(R.id.naslovnaTelefonTextView);
        ImageView slikaNaslovnice = (ImageView) findViewById(R.id.naslovnaSlikaRestorana);
        String imeR = PrivremenaBaza.baza.procitajInformaciju("imeRestorana");
        if(imeR == null){ imeR = PrivremenaBaza.baza.procitajInformaciju("imeRestoranaDefault");}
        if(imeR == null){ imeR = "Ne radi"; }
        String adresaR = PrivremenaBaza.baza.procitajInformaciju("adresaRestorana");
        if(adresaR == null){ adresaR = PrivremenaBaza.baza.procitajInformaciju("adresaRestoranaDefault");}
        if(adresaR == null){ adresaR = "Ne radi"; }
        String opisR = PrivremenaBaza.baza.procitajInformaciju("opisRestorana");
        if(opisR == null){ opisR = PrivremenaBaza.baza.procitajInformaciju("opisRestoranaDefault");}
        if(opisR == null){ opisR = "Ne radi"; }
        String imeV = PrivremenaBaza.baza.procitajInformaciju("vlasnikRestorana");
        if(imeV == null){ imeV = PrivremenaBaza.baza.procitajInformaciju("vlasnikRestoranaDefault");}
        if(imeV == null){ imeV = "Ne radi"; }
        String radnoV = PrivremenaBaza.baza.procitajInformaciju("radnoVrijemeRestorana");
        if(radnoV == null){ radnoV = PrivremenaBaza.baza.procitajInformaciju("radnoVrijemeRestoranaDefault");}
        if(radnoV == null){ radnoV = "Ne radi"; }
        String tel = PrivremenaBaza.baza.procitajInformaciju("telefon");
        if(tel == null) { tel = PrivremenaBaza.baza.procitajInformaciju("telefonDefault"); }
        if(tel == null) { tel = "Ne radi"; }
        String imagePath = PrivremenaBaza.baza.procitajInformaciju("slikaNaslovnice");
        if(imagePath!=null){
            try {
                slikaNaslovnice.setImageURI(Uri.parse(imagePath));
            }
            catch (Exception e){
                slikaNaslovnice.setImageResource(R.drawable.restoran);
            }
        }
        imeRestorana.setText(imeR);
        adresaRestorana.setText(adresaR);
        opisRestorana.setText(opisR);
        imeVlasnika.setText("Vlasnik: " + imeV);
        radnoVrijeme.setText("Radno vrijeme: " + radnoV);
        telefon.setText("Telefon: " + tel);
    }

    private void postaviInfoUredjivanje(){
        EditText imeRestorana = (EditText) findViewById(R.id.naslovnaImeRestoranaEditTextANU);
        EditText adresaRestorana = (EditText) findViewById(R.id.naslovnaAdresaRestoranaEditTextANU);
        EditText opisRestorana = (EditText) findViewById(R.id.naslovnaOpisRestoranaEditTextAMU);
        EditText imeVlasnika = (EditText) findViewById(R.id.naslovnaImeVlasnikaEditTextAMU);
        EditText radnoVrijeme = (EditText) findViewById(R.id.naslovnaRadnoVrijemeEditTextAMU);
        EditText telefon = (EditText) findViewById(R.id.naslovnaTelefonEditTextAMU);
        ImageView slikaNaslovnice = (ImageView) findViewById(R.id.naslovnaSlikaRestorana);
        String imeR = PrivremenaBaza.baza.procitajInformaciju("imeRestorana");
        if(imeR == null){ imeR = PrivremenaBaza.baza.procitajInformaciju("imeRestoranaDefault");}
        if(imeR == null){ imeR = "Ne radi"; }
        String adresaR = PrivremenaBaza.baza.procitajInformaciju("adresaRestorana");
        if(adresaR == null){ adresaR = PrivremenaBaza.baza.procitajInformaciju("adresaRestoranaDefault");}
        if(adresaR == null){ adresaR = "Ne radi"; }
        String opisR = PrivremenaBaza.baza.procitajInformaciju("opisRestorana");
        if(opisR == null){ opisR = PrivremenaBaza.baza.procitajInformaciju("opisRestoranaDefault");}
        if(opisR == null){ opisR = "Ne radi"; }
        String imeV = PrivremenaBaza.baza.procitajInformaciju("vlasnikRestorana");
        if(imeV == null){ imeV = PrivremenaBaza.baza.procitajInformaciju("vlasnikRestoranaDefault");}
        if(imeV == null){ imeV = "Ne radi"; }
        String radnoV = PrivremenaBaza.baza.procitajInformaciju("radnoVrijemeRestorana");
        if(radnoV == null){ radnoV = PrivremenaBaza.baza.procitajInformaciju("radnoVrijemeRestoranaDefault");}
        if(radnoV == null){ radnoV = "Ne radi"; }
        String tel = PrivremenaBaza.baza.procitajInformaciju("telefon");
        if(tel == null) { tel = PrivremenaBaza.baza.procitajInformaciju("telefonDefault"); }
        if(tel == null) { tel = "Ne radi"; }
        String imagePath = PrivremenaBaza.baza.procitajInformaciju("slikaNaslovnice");
        if(imagePath!=null){
            try {
                slikaNaslovnice.setImageURI(Uri.parse(imagePath));
            }
            catch (Exception e){
                slikaNaslovnice.setImageResource(R.drawable.restoran);
            }
        }
        imeRestorana.setText(imeR);
        adresaRestorana.setText(adresaR);
        opisRestorana.setText(opisR);
        imeVlasnika.setText(imeV);
        radnoVrijeme.setText(radnoV);
        telefon.setText(tel);
    }

    public void pokreniPrikazKategorija(View view){
        Intent prikazKategorija = new Intent(this, NarucivanjeActivity.class);
        prikazKategorija.putExtra("mod", 1);
        startActivity(prikazKategorija);
    }

    public void pokreniPrikazKategorijaDjelatnik(View view){
        Intent prikazKategorijaDjelatnik = new Intent(this, NarucivanjeDjelatnikActivity.class);
        prikazKategorijaDjelatnik.putExtra("djelatnik",logiraniDjelatnik.getUsername());
        startActivity(prikazKategorijaDjelatnik);
    }

    public void pokreniPrikazKosarice(View view){
        Intent prikazKosarice = new Intent(this, NarucivanjeActivity.class);
        prikazKosarice.putExtra("mod", 3);
        startActivity(prikazKosarice);
    }

    public void pokreniPrikazKomentara(View view){
        Intent prikazKomentara = new Intent(this, KomentariActivity.class);
        prikazKomentara.putExtra("Djelatnik", jeliLogirano);
        startActivity(prikazKomentara);
    }

    private double izracunajOcjenu(){
        Recenzija[] recenzije = new Recenzija[0];
        try {
            recenzije = PrivremenaBaza.baza.izvuciSveRecenzije();
        }catch(Exception e){

        }
        int brojOcjena = 0;
        for(Recenzija recenzija : recenzije){
            if(recenzija.getOcjena() != null){
                prosjecnaOcjenaRestorana += recenzija.getOcjena();
                brojOcjena++;
            }
        }
        if(brojOcjena != 0){
            prosjecnaOcjenaRestorana /= brojOcjena;
        }else{
            prosjecnaOcjenaRestorana = 0;
        }
        return prosjecnaOcjenaRestorana;
    }



    //LOGOUT STUFF HERE -------------------------
    public void logout(View v){
        initKlijentMod();
    }

    //LOGIN STUFF HERE --------------------------
    public void pokreniLogin(View view){
        loginProcess();
    }
    private PopupWindow loginPopup = null;
    private Button buttonNastavi = null;
    private Button buttonNazad = null;
    private EditText editTextUsername = null;
    private EditText editTextPassword = null;
    private View loginPopupView = null;
    private void loginProcess(){
        loginPopupEnabled = true;
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loginPopupView = inflater.inflate(R.layout.login_popup_window,(ViewGroup)findViewById(R.id.linearLayoutLoginPopup));
        loginPopup = new PopupWindow(loginPopupView, 100, 100, true);
        loginPopup.showAtLocation(this.findViewById(R.id.linearLayoutMainANK), Gravity.CENTER, 0, 0);
        loginPopupView.measure(View.MeasureSpec.UNSPECIFIED,View.MeasureSpec.UNSPECIFIED);
        loginPopup.update(findViewById(R.id.linearLayoutMainANK).getWidth(), loginPopupView.findViewById(R.id.linearLayoutLoginPopup).getMeasuredHeight());
        loginPopup.getContentView().setFocusableInTouchMode(true);
        loginPopup.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                        loginPopup.dismiss();
                        loginPopupEnabled = false;
                        return true;
                }
                return false;
            }
        });
        buttonNastavi = (Button) loginPopupView.findViewById(R.id.buttonNastaviLPW);
        buttonNazad = (Button) loginPopupView.findViewById(R.id.buttonNazadLPW);
        editTextUsername = (EditText) loginPopupView.findViewById(R.id.editTextUsernameLPW);
        editTextPassword = (EditText) loginPopupView.findViewById(R.id.editTextPasswordLPW);
        View.OnClickListener loginOnClickListener = getFirstPopupOnClickListener();
        buttonNastavi.setOnClickListener(loginOnClickListener);
        buttonNazad.setOnClickListener(loginOnClickListener);
    }

    private View.OnClickListener getFirstPopupOnClickListener(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId() == buttonNastavi.getId()){
                    if(checkUserData()){
                        loginPopupEnabled = false;
                        loginPopup.dismiss();
                        initDjelatnikMod();
                    }
                    else{
                        prijavaGreske("Upisali ste krive podatke ili korisnik nije aktivan!");
                    }
                }
                else if(v.getId() == buttonNazad.getId()){
                    loginPopupEnabled = false;
                    loginPopup.dismiss();
                }
            }
        };
    }

    private boolean checkUserData(){
        Djelatnik trazeniDjelatnik = PrivremenaBaza.baza.uhvatiDjelatnikaPoUsernamePassword(editTextUsername.getText().toString(), editTextPassword.getText().toString());
        if(trazeniDjelatnik != null && trazeniDjelatnik.jeAktivan()){
            logiraniDjelatnik = trazeniDjelatnik;
            jeliLogirano = true;
            return true;
        }
        return false;
    }

    private PopupWindow errorPopup = null;
    private View errorPopupView = null;
    private TextView textViewError = null;
    private Button buttonBack = null;
    private void prijavaGreske(String greska){
        errorPopupEnabled = true;
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        errorPopupView = inflater.inflate(R.layout.error_popup_window,(ViewGroup)findViewById(R.id.linearLayoutErrorPopup));
        errorPopup = new PopupWindow(errorPopupView, 100, 100, true);
        errorPopup.showAtLocation(this.findViewById(R.id.linearLayoutMainANK), Gravity.CENTER, 0, 0);
        errorPopup.getContentView().setFocusableInTouchMode(true);
        errorPopup.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    errorPopupEnabled = false;
                    errorPopup.dismiss();
                    return true;
                }
                return false;
            }
        });

        errorPopupView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        errorPopup.update(findViewById(R.id.linearLayoutMainANK).getWidth(), errorPopupView.findViewById(R.id.linearLayoutErrorPopup).getMeasuredHeight());
        textViewError = (TextView)errorPopupView.findViewById(R.id.textViewErrorEPW);
        textViewError.setText(greska);
        buttonBack = (Button)errorPopupView.findViewById(R.id.buttonBackEPW);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorPopupEnabled = false;
                errorPopup.dismiss();
            }
        });
    }

    private PopupWindow myUserPopup = null;
    private View myUserPopupView = null;
    private TextView textViewMyUserUsername = null;
    private TextView textViewMyUserPassword = null;
    private TextView textViewVlasnik = null;
    public void myUserPopup(View v){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        myUserPopupView = inflater.inflate(R.layout.myuser_popup_window,(ViewGroup)findViewById(R.id.linearLayoutMyUserPopup));
        myUserPopup = new PopupWindow(myUserPopupView, 100, 100, true);
        myUserPopup.showAtLocation(this.findViewById(R.id.linearLayoutMainAN), Gravity.CENTER, 0, 0);
        myUserPopupView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        myUserPopup.update(findViewById(R.id.linearLayoutMainAN).getWidth(), myUserPopupView.findViewById(R.id.linearLayoutMyUserPopup).getMeasuredHeight());
        myUserPopup.getContentView().setFocusableInTouchMode(true);
        myUserPopup.getContentView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    myUserPopup.dismiss();
                    return true;
                }
                return false;
            }
        });
        textViewMyUserUsername = (TextView)myUserPopupView.findViewById(R.id.textViewUsernameMUPW);
        textViewMyUserPassword = (TextView)myUserPopupView.findViewById(R.id.textViewPasswordMUPW);
        textViewVlasnik = (TextView)myUserPopupView.findViewById(R.id.textViewVlasnikMUPW);
        textViewMyUserUsername.setText(textViewMyUserUsername.getText() + " " + logiraniDjelatnik.getUsername());
        textViewMyUserPassword.setText(textViewMyUserPassword.getText() + " " + logiraniDjelatnik.getPassword());
        if(logiraniDjelatnik.jeVlasnik()){
            textViewVlasnik.setText(textViewVlasnik.getText() + " :)");
        } else {
            textViewVlasnik.setText(textViewVlasnik.getText() + " :(");
        }
    }
    public void dismissMyUserPopup(View v){
        myUserPopup.dismiss();
    }

    public void manageUsers(View v){
        Intent manageUsersIntent = new Intent (this, ManageUsersActivity.class);
        manageUsersIntent.putExtra("vlasnik", logiraniDjelatnik.jeVlasnik());
        manageUsersIntent.putExtra("korisnik", logiraniDjelatnik.getUsername());
        startActivity(manageUsersIntent);
    }

    //UREDI NASLOVNICU STUFF HERE

    private Button urediNaslovnicu = null;
    private void initInitUredjivanja(){
        urediNaslovnicu = new Button(getApplicationContext());
        urediNaslovnicu.setText("UREDI NASLOVNICU");
        urediNaslovnicu.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        urediNaslovnicu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initUredjivanje();
            }
        });
        LinearLayout linearLayoutMainAN = (LinearLayout) findViewById(R.id.linearLayoutMainAN);
        linearLayoutMainAN.addView(urediNaslovnicu);
    }
    private static final int SELECT_PICTURE = 1;
    public void odaberiSliku(View v){
        Intent fileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        fileIntent.setType("image/*");
        fileIntent.setAction(Intent.ACTION_GET_CONTENT);
        try{
            startActivityForResult(Intent.createChooser(fileIntent, "Odaberi sliku."), SELECT_PICTURE);
        } catch(Exception e) {}
    }
    private Uri selectedImageUri = null;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK){
            if(requestCode == SELECT_PICTURE){
                selectedImageUri = data.getData();
                if(PrivremenaBaza.baza.procitajInformaciju("slikaNaslovnice") != null){
                    PrivremenaBaza.baza.izbrisiInformaciju("slikaNaslovnice");
                }
                PrivremenaBaza.baza.spremiInformaciju("slikaNaslovnice", selectedImageUri.toString());
                ((ImageView)findViewById(R.id.naslovnaSlikaRestorana)).setImageURI(selectedImageUri);
            }
        }
    }
    public void pohraniUredjivanje(View v){
        EditText imeRestorana = (EditText) findViewById(R.id.naslovnaImeRestoranaEditTextANU);
        EditText adresaRestorana = (EditText) findViewById(R.id.naslovnaAdresaRestoranaEditTextANU);
        EditText opisRestorana = (EditText) findViewById(R.id.naslovnaOpisRestoranaEditTextAMU);
        EditText imeVlasnika = (EditText) findViewById(R.id.naslovnaImeVlasnikaEditTextAMU);
        EditText radnoVrijeme = (EditText) findViewById(R.id.naslovnaRadnoVrijemeEditTextAMU);
        EditText telefon = (EditText) findViewById(R.id.naslovnaTelefonEditTextAMU);
        if(PrivremenaBaza.baza.procitajInformaciju("imeRestorana") != null) {PrivremenaBaza.baza.izbrisiInformaciju("imeRestorana");}
        PrivremenaBaza.baza.spremiInformaciju("imeRestorana", imeRestorana.getText().toString());
        if(PrivremenaBaza.baza.procitajInformaciju("adresaRestorana") != null) {PrivremenaBaza.baza.izbrisiInformaciju("adresaRestorana");}
        PrivremenaBaza.baza.spremiInformaciju("adresaRestorana", adresaRestorana.getText().toString());
        if(PrivremenaBaza.baza.procitajInformaciju("opisRestorana") != null) {PrivremenaBaza.baza.izbrisiInformaciju("opisRestorana");}
        PrivremenaBaza.baza.spremiInformaciju("opisRestorana", opisRestorana.getText().toString());
        if(PrivremenaBaza.baza.procitajInformaciju("vlasnikRestorana") != null) {PrivremenaBaza.baza.izbrisiInformaciju("vlasnikRestorana");}
        PrivremenaBaza.baza.spremiInformaciju("vlasnikRestorana", imeVlasnika.getText().toString());
        if(PrivremenaBaza.baza.procitajInformaciju("radnoVrijemeRestorana") != null) {PrivremenaBaza.baza.izbrisiInformaciju("radnoVrijemeRestorana");}
        PrivremenaBaza.baza.spremiInformaciju("radnoVrijemeRestorana", radnoVrijeme.getText().toString());
        if(PrivremenaBaza.baza.procitajInformaciju("telefon") != null){PrivremenaBaza.baza.izbrisiInformaciju("telefon");}
        PrivremenaBaza.baza.spremiInformaciju("telefon", telefon.getText().toString());
        initDjelatnikMod();
    }

    public void odustaniUredjivanje(View v){
        initDjelatnikMod();
    }


    //STATISTIKA STUFF HERE ---------------------
    private Button buttonStatistika = null;
    private void initInitStatistike(){
        buttonStatistika = new Button(getApplicationContext());
        buttonStatistika.setText("STATISTIKA");
        buttonStatistika.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        buttonStatistika.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent statistikaIntent = new Intent(NaslovnaActivity.this, StatistikaActivity.class);
                startActivity(statistikaIntent);
            }
        });
        LinearLayout linearLayoutMainAN = (LinearLayout) findViewById(R.id.linearLayoutMainAN);
        linearLayoutMainAN.addView(buttonStatistika);
    }

    //NARUDZBE STUFF HERE -----------------------
    private LinearLayout linearLayoutNarudzbe = null;
    private LinkedList<Narudzba> listaNarudzbi = new LinkedList<>();
    private LinkedList<Narudzba> listaNarudzbiNepreuzeta = new LinkedList<>();
    private LinkedList<Narudzba> listaNarudzbiPreuzeta = new LinkedList<>();
    private LinkedList<Narudzba> listaNarudzbiSpremna = new LinkedList<>();
    private LinkedList<Narudzba> listaNarudzbiOdnesena = new LinkedList<>();
    private LinkedList<Narudzba> listaNarudzbiIsporuceno = new LinkedList<>();
    private LinkedList<NarudzbaDisplay> listaDisplayaNarudzbi = new LinkedList<>();

    private void initNarudzbeDrugiDio(){
        linearLayoutNarudzbe = (LinearLayout)findViewById(R.id.linearLayoutNarudzbe);
        listaNarudzbi = (LinkedList)PrivremenaBaza.baza.vratiSveNarudzbe();
        listaNarudzbiNepreuzeta.clear();
        listaNarudzbiPreuzeta.clear();
        listaNarudzbiSpremna.clear();
        listaNarudzbiOdnesena.clear();
        listaNarudzbiIsporuceno.clear();
        for(Narudzba trenutnaNarudzba : listaNarudzbi){
            if(trenutnaNarudzba.jeIsporucena()){
                listaNarudzbiIsporuceno.add(trenutnaNarudzba);
            } else if (trenutnaNarudzba.jeOdnesena()){
                listaNarudzbiOdnesena.add(trenutnaNarudzba);
            } else if (trenutnaNarudzba.jeSpremna()){
                listaNarudzbiSpremna.add(trenutnaNarudzba);
            } else if (trenutnaNarudzba.jePreuzeta()){
                listaNarudzbiPreuzeta.add(trenutnaNarudzba);
            } else {
                listaNarudzbiNepreuzeta.add(trenutnaNarudzba);
            }
        }

        for(Narudzba trenutnaNarudzba : listaNarudzbiNepreuzeta){
            listaDisplayaNarudzbi.add(new NarudzbaDisplay(trenutnaNarudzba, "NEPREUZETA"));
        }
        for(Narudzba trenutnaNarudzba : listaNarudzbiPreuzeta){
            listaDisplayaNarudzbi.add(new NarudzbaDisplay(trenutnaNarudzba, "PREUZETA"));
        }
        for(Narudzba trenutnaNarudzba : listaNarudzbiSpremna){
            listaDisplayaNarudzbi.add(new NarudzbaDisplay(trenutnaNarudzba, "SPREMNA"));
        }
        for(Narudzba trenutnaNarudzba : listaNarudzbiOdnesena){
            listaDisplayaNarudzbi.add(new NarudzbaDisplay(trenutnaNarudzba, "ODNESENA"));
        }
        for(Narudzba trenutnaNarudzba : listaNarudzbiIsporuceno){
            listaDisplayaNarudzbi.add(new NarudzbaDisplay(trenutnaNarudzba, "isporucena"));
        }
    }
    private Button buttonPreuzimanjeNarudzbi = null;
    public void preuzimanjeNarudzbi(View v){
        initNarudzbe();
    }

    private class NarudzbaDisplay {
        private LinearLayout linearLayoutNarudzbaDisplay = null;
        private TextView textViewIdNarudzbe = null;
        private TextView textViewStatusNarudzbe = null;
        private Button buttonDetalji = null;
        private Button buttonPotvrdi = null;
        private Narudzba ovaNarudzba = null;
        private String statusNarudzbe = null;
        NarudzbaDisplay(final Narudzba trazenaNarudzba, final String statusNarudzbe){
            ovaNarudzba = trazenaNarudzba;
            this.statusNarudzbe = statusNarudzbe;
            linearLayoutNarudzbaDisplay = new LinearLayout(getApplicationContext());
            linearLayoutNarudzbaDisplay.setOrientation(LinearLayout.HORIZONTAL);
            linearLayoutNarudzbaDisplay.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            linearLayoutNarudzbaDisplay.setWeightSum(4);

            textViewIdNarudzbe = new TextView(getApplicationContext());
            textViewIdNarudzbe.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.8f));
            textViewIdNarudzbe.setText("ID: " + trazenaNarudzba.getId());
            textViewIdNarudzbe.setTextSize(12);
            textViewIdNarudzbe.setTextColor(Color.BLACK);

            textViewStatusNarudzbe = new TextView(getApplicationContext());
            textViewStatusNarudzbe.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.2f));
            textViewStatusNarudzbe.setText("STATUS: " + statusNarudzbe);
            textViewStatusNarudzbe.setTextSize(12);
            textViewStatusNarudzbe.setTextColor(Color.BLACK);

            View.OnClickListener narudzbaDisplayOnClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getId() == buttonDetalji.getId()){
                        linearLayoutNarudzbe.removeAllViews();
                        trenutnaNarudzbaDetalji = trazenaNarudzba;
                        statusNarudzbeDetalji = statusNarudzbe;
                        initDetaljiNarudzbeMod();
                    } else if(v.getId() == buttonPotvrdi.getId()){
                        if(!trazenaNarudzba.jePreuzeta()){
                            PrivremenaBaza.baza.preuzmiNarudzbu(trazenaNarudzba, logiraniDjelatnik.getUsername());
                            textViewStatusNarudzbe.setText("STATUS: PREUZETA");
                            buttonPotvrdi.setText("SPREMNA");
                        } else if(!trazenaNarudzba.jeSpremna()){
                            PrivremenaBaza.baza.narudzbaSpremna(trazenaNarudzba, logiraniDjelatnik.getUsername());
                            textViewStatusNarudzbe.setText("STATUS: SPREMNA");
                            buttonPotvrdi.setText("ODNESENA");
                        } else if(!trazenaNarudzba.jeOdnesena()){
                            PrivremenaBaza.baza.odnesiNarudzbu(trazenaNarudzba, logiraniDjelatnik.getUsername());
                            textViewStatusNarudzbe.setText("STATUS: ODNESENA");
                            buttonPotvrdi.setText("ISPORUČENA");
                        } else if(!trazenaNarudzba.jeIsporucena()) {
                            PrivremenaBaza.baza.narudzbaIsporucena(trazenaNarudzba, logiraniDjelatnik.getUsername());
                            textViewStatusNarudzbe.setText("STATUS: ISPORUČENA");
                            buttonPotvrdi.setText("-");
                            buttonPotvrdi.setEnabled(false);
                        }
                    }
                }
            };

            buttonDetalji = new Button(getApplicationContext());
            buttonDetalji.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1));
            buttonDetalji.setText("DETALJI");
            buttonDetalji.setTextSize(10);
            buttonDetalji.setId(trazenaNarudzba.getId() * 2);
            buttonDetalji.setOnClickListener(narudzbaDisplayOnClickListener);

            buttonPotvrdi = new Button(getApplicationContext());
            buttonPotvrdi.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1));
            buttonPotvrdi.setTextSize(10);
            buttonPotvrdi.setId(trazenaNarudzba.getId()*2 + 1);
            if(statusNarudzbe.equalsIgnoreCase("nepreuzeta")){
                buttonPotvrdi.setText("PREUZMI");
            } else if (statusNarudzbe.equalsIgnoreCase("preuzeta")){
                buttonPotvrdi.setText("SPREMNA");
            } else if (statusNarudzbe.equalsIgnoreCase("spremna")){
                buttonPotvrdi.setText("ODNESENA");
            } else if (statusNarudzbe.equalsIgnoreCase("odnesena")){
                buttonPotvrdi.setText("ISPORUCENA");
            } else {
                buttonPotvrdi.setEnabled(false);
                buttonPotvrdi.setText("-");
            }
            buttonPotvrdi.setOnClickListener(narudzbaDisplayOnClickListener);

            linearLayoutNarudzbaDisplay.addView(textViewIdNarudzbe);
            linearLayoutNarudzbaDisplay.addView(textViewStatusNarudzbe);
            linearLayoutNarudzbaDisplay.addView(buttonDetalji);
            linearLayoutNarudzbaDisplay.addView(buttonPotvrdi);

            linearLayoutNarudzbe.addView(linearLayoutNarudzbaDisplay);
        }
    }


    private Narudzba trenutnaNarudzbaDetalji = null;
    private String statusNarudzbeDetalji;
    private NarudzbaJelo[] sadrzajNarudzbe;
    private void postaviDetaljiNarudzbe(){
        LinearLayout linearLayoutMainDN = (LinearLayout)findViewById(R.id.linearLayoutMainDN);

        sadrzajNarudzbe = PrivremenaBaza.baza.jelaNarudzbe(trenutnaNarudzbaDetalji.getId());
        TextView textViewIdNarudzbe = (TextView)findViewById(R.id.textViewIDN_DN);
        TextView textViewStatusNarudzbe = (TextView)findViewById(R.id.textViewStatus_DN);
        TextView textViewIdKlijenta = (TextView)findViewById(R.id.textViewIDK_DN);
        TextView textViewTelefon = (TextView)findViewById(R.id.textViewTelefon_DN);
        TextView textViewAdresa = (TextView)findViewById(R.id.textViewAdresa_DN);
        TextView textViewEmail = (TextView)findViewById(R.id.textViewEmail_DN);
        TextView textViewKat = (TextView)findViewById(R.id.textViewKat_DN);
        TextView textViewDjelatnik = (TextView)findViewById(R.id.textViewDjelatnik_DN);
        TextView textViewDatum = (TextView)findViewById(R.id.textViewDatum_DN);

        textViewIdNarudzbe.setText(trenutnaNarudzbaDetalji.getId().toString());
        textViewStatusNarudzbe.setText(statusNarudzbeDetalji);
        textViewIdKlijenta.setText(PrivremenaBaza.baza.dohvatePodatkeZaID(trenutnaNarudzbaDetalji.getPodaciID()).getId().toString());
        textViewTelefon.setText(PrivremenaBaza.baza.dohvatePodatkeZaID(trenutnaNarudzbaDetalji.getPodaciID()).getTelefon());
        textViewAdresa.setText(PrivremenaBaza.baza.dohvatePodatkeZaID(trenutnaNarudzbaDetalji.getPodaciID()).getAdresa());
        textViewEmail.setText(PrivremenaBaza.baza.dohvatePodatkeZaID(trenutnaNarudzbaDetalji.getPodaciID()).getEmail());
        textViewKat.setText(String.valueOf(PrivremenaBaza.baza.dohvatePodatkeZaID(trenutnaNarudzbaDetalji.getPodaciID()).getKat()));
        if(!trenutnaNarudzbaDetalji.jePreuzeta()) {
            textViewDjelatnik.setText("NITKO");
        } else {
            textViewDjelatnik.setText(trenutnaNarudzbaDetalji.getDjelatnik());
        }
        textViewDatum.setText(trenutnaNarudzbaDetalji.getVrijeme());
        double ukupnaCijena = 0;
        for(NarudzbaJelo trenutnaNarudzbaJelo : sadrzajNarudzbe){
            LinearLayout linearLayoutNarudzbaJelo = new LinearLayout(getApplicationContext());
            linearLayoutNarudzbaJelo.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            linearLayoutNarudzbaJelo.setWeightSum(5);
            linearLayoutNarudzbaJelo.setOrientation(LinearLayout.HORIZONTAL);

            TextView textViewNazivJela = new TextView(getApplicationContext());
            textViewNazivJela.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.6f));
            textViewNazivJela.setText(PrivremenaBaza.baza.vratiJelo(trenutnaNarudzbaJelo.getJeloID()).getIme());
            ((LinearLayout.LayoutParams)textViewNazivJela.getLayoutParams()).setMargins(10, 0, 0, 0);
            textViewNazivJela.setTextColor(Color.LTGRAY);
            linearLayoutNarudzbaJelo.addView(textViewNazivJela);

            TextView textViewKolicinaJela = new TextView(getApplicationContext());
            textViewKolicinaJela.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.2f));
            textViewKolicinaJela.setText(String.valueOf(trenutnaNarudzbaJelo.getKolicina()));
            textViewKolicinaJela.setTextColor(Color.LTGRAY);
            linearLayoutNarudzbaJelo.addView(textViewKolicinaJela);

            TextView textViewCijenaJela = new TextView(getApplicationContext());
            textViewCijenaJela.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.1f));
            textViewCijenaJela.setText(String.format("%.2f", PrivremenaBaza.baza.vratiJelo(trenutnaNarudzbaJelo.getJeloID()).getCijena()));
            textViewCijenaJela.setTextColor(Color.LTGRAY);
            linearLayoutNarudzbaJelo.addView(textViewCijenaJela);

            TextView textViewUkupnaCijena = new TextView(getApplicationContext());
            textViewUkupnaCijena.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.2f));
            double ukCijena = trenutnaNarudzbaJelo.getKolicina() * PrivremenaBaza.baza.vratiJelo(trenutnaNarudzbaJelo.getJeloID()).getCijena();
            textViewUkupnaCijena.setText(String.format("%.2f", ukCijena));
            textViewUkupnaCijena.setTextColor(Color.LTGRAY);
            linearLayoutNarudzbaJelo.addView(textViewUkupnaCijena);

            ukupnaCijena += ukCijena;
            linearLayoutMainDN.addView(linearLayoutNarudzbaJelo);

            TextView textViewKarakteristike = new TextView(getApplicationContext());
            textViewKarakteristike.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textViewKarakteristike.setText("KARAKTERISTIKE: ");
            textViewKarakteristike.setTextColor(Color.LTGRAY);
            ((LinearLayout.LayoutParams)textViewKarakteristike.getLayoutParams()).setMargins(10, 0, 0, 0);
            int i = 0;
            for(String karakteristika : trenutnaNarudzbaJelo.getKarakteristike()){
                if(karakteristika.equals("")){break;}
                if(i==0){textViewKarakteristike.setText(textViewKarakteristike.getText() + karakteristika);}
                else {textViewKarakteristike.setText(textViewKarakteristike.getText() + ", " + karakteristika);}
                ++i;
            }
            if(i!=0){linearLayoutMainDN.addView(textViewKarakteristike);}
        }
        TextView textViewUkupno = new TextView(getApplicationContext());
        textViewUkupno.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        ((LinearLayout.LayoutParams)textViewUkupno.getLayoutParams()).setMargins(20, 0, 0, 0);
        textViewUkupno.setTextColor(Color.BLACK);
        textViewUkupno.setTextSize(18);
        textViewUkupno.setText(String.format("UKUPNO: %.2f", ukupnaCijena));

        linearLayoutMainDN.addView(textViewUkupno);
    }
}
